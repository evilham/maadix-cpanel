Maadix Control Panel
=============

A graphical interface written in django, for the management of Maadix services.

# Description  
 
Simplified interface to manage LDAP entries. It's based on Phamm schema and adds different Organization Unit for several additional purposes and services.
It doesn't store the LDAP administration password. Instead it binds using user input password.
It has been developed for MaadiX systems, but it can be reused and adapted for any LDAP installation and environment.  

# Features  

This Sotfware allows Managing entries in ldap directory.  
 
* create and manage  users: password, authorized services, associated email account
* Create and manage domains: add, delete, edit domains, asign webmaster, activate email server (email server not included. need to have postfix + dovecot + ldap), activate DkIM (need to install opendkim)  
* DNS check for domains  
* Create and manage email accounts: add, delete, edit email accounts, activate forward & Vacation (need to install gnawrl for service to work) edit automatic message  
* Available user profiles are: ldap administrator, postmaster, email accounts. Each profile can login wit following permissions:  
** Ldap Adinistratos: can manage any entry  
** Postmaster: can edit its own password and manage all the email accounts of its domain  
** Email accounts: can edit their own account (password, forward, Vacation)  
* recover password fro ldap administrator  
* Set a sendermail: this will be the account used to send notifications from server  
* Show details about system (CPU, RaM. Disk Usage)  
* Manage Applications - These feature will only work with MaadiX environment. Available applications are pubblished in an external API which the control panel connects with using a user and password.
When users check an application for its installation a value is recorded in ldap and puppet will be run to perform the installation.
* 2FA



These are the included Objects in LDAP Tree:

```
# Objeto 1: cn=youradminusername,dc=example,dc=tld
dn: cn=youradminusername,dc=example,dc=tld
cn: youradminusername
description: LDAP administrator
email: youremail@example.com
objectclass: organizationalRole
objectclass: simpleSecurityObject
objectclass: extensibleObject
status: active
userpassword: {ssha}YOUR_PASSWORD_HASH

# Objeto 2: o=hosting,dc=example,dc=tld
dn: o=hosting,dc=example,dc=tld
o: hosting
objectclass: organization

# Objeto 3: ou=api,dc=example,dc=tld
# This is an external api from where hosts can read available applications or updates
# It is not necessary if you just want to manage the Ldap Directory or can be customized with own api

dn: ou=api,dc=example,dc=tld
host: https://your.api.url/
objectclass: organizationalUnit
objectclass: extensibleObject
objectclass: top
ou: api
uid: YOUR_API_USER
userpassword: YOUR_USER_API_PAAWORD

# Objeto 4: ou=cpanel,dc=example,dc=tld
dn: ou=cpanel,dc=example,dc=tld
creationdate: TIMESTAMP
info: NAME_OF_INSTALLATION_RELEASE
objectclass: organizationalUnit
objectclass: extensibleObject
ou: cpanel
status: ready
type: NAME_OF_CURRENT_RELEASE

# Objeto 5: ou=credentials,dc=example,dc=tld
dn: ou=credentials,dc=example,dc=tld
objectclass: organizationalUnit
ou: credentials

# Objeto 6: ou=groups,dc=example,dc=tld
dn: ou=groups,dc=example,dc=tld
objectclass: organizationalUnit
ou: groups

# Objeto 7: ou=People,dc=example,dc=tld
dn: ou=People,dc=example,dc=tld
objectclass: organizationalUnit
ou: People

# Objeto 8: ou=sendermail,dc=example,dc=tld
dn: ou=sendermail,dc=example,dc=tld
cn: senderemail@example.com
objectclass: organizationalUnit
objectclass: top
objectclass: metaInfo
ou: sendermail

```

# Requirements

* LDAP Server
* Phamm schema + additional Objects
* Python3 python3-dev  apache2-dev libpq-dev
* Wsgi-express
* libnss-ldapd libpam-ldapd

# Installation  

```
# apt-get install python3-dev  apache2-dev libpq-dev
$ git clone https://gitlab.com/MaadiX/maadix-cpanel.git
$ cd maadix-cpanel
$ virtualenv -p python3 env
$ source env/bin/activate
$ pip install -r requirements.txt

# To install in subpath
Edit settings.py
FORCE_SCRIPT_NAME = '/YOUR_PATHNAME' # To change the subpath installation. Default is cpanel
FORCE_SCRIPT_NAME = '' # For installation without subpath - Not Checked

#Edit static/mxcp/js/main.js
function get_installation_path(){
  //var path = ''; // For installation without subpath
  var path = '/YOUR_PATHNAME'; # For installation with subpath  - default is cpanel
  return path;
}

# apache proxy configuration - In your virtualhost
ProxyPass /django http://localhost:8000/
ProxyPassReverse /django http://localhost:8000/

    ProxyRequests Off
    ProxyPreserveHost on
    <Proxy http://localhost:8000/ >
        Options FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
    </Proxy>
```
## To run django in development:
mod_wsgi-express start-server --url-alias /static ./static mxcp/wsgi.py

## To run django as another user (eg: myuser) - Need to be root:      
mod_wsgi-express start-server --user=myuser --group mygroup --debug-mode --reload-on-changes --url-alias /static ./static mxcp/wsgi.py

 This will generate the SECRET_KEY for the installation in the file mxcp/key_file.py 
 This file must be readable by the user who is running Django 

-rw------- 1 myuser sudo   65 ene 31 09:11 key_file.py


# System settings

Following configurations are out of the scoe of this repository. Anyway , we include these additional system settings for Debian stretch in order to enable ldap authetication on your system and to jail sftpusers.    
You may use it or check some external tutorial in order to enable these features, according to your system.  

## Users
The sftpusers can be jailed in their own home. For that some Linux System settings are required

* Create a group named sftpusers
* Edit /etc/ssh/sshd_cnfig to jail users in sftpusers group 

```
AcceptEnv LANG LC_*
ChallengeResponseAuthentication no
PrintMotd no
Subsystem sftp /usr/lib/openssh/sftp-server -u 0002
UsePAM yes 
X11Forwarding yes 
Match Group sftpusers
    AllowAgentForwarding no
    AllowTcpForwarding no
    ChrootDirectory /home/sftpusers
    ForceCommand internal-sftp
    PermitTunnel no
    X11Forwarding no


```


## Apache Vhosts
user can add domains through the interface which will be set up as Apache Virtualhosts by a script. If you want to enable this feature you need to:

* Dowmload the following script and place it somewhere in your system. make it only readable and executable by root:
git clone https://gitlab.com/MaadiX/system-cron

* Vhosts created by the script will be located in /etc/apache/ldap-enabled/ so these steps are required:
 -  Create folder /etc/apache/ldap-enabled/
 -  In /etc/apache/apache2.conf add

IncludeOptional ldap-enabled/*.conf


* Add a cronjob to execute the script:
*/5 * * * * /bin/sleep `/usr/bin/numrandom /0..60/`s ; /bin/bash /path/of/script/system-cron/ldapsearch.sh > /dev/null 2>&1


## Enable ldap authentication  

### nscld
Configure /etc/nslcd.conf according to your system. An example to allow users in ou=People,dc=example,dc=tld to authenticate to servcies (ssh, openvpn, apache etc) :


```

# module: 'nslcd'

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The location at which the LDAP server(s) should be reachable.
uri ldapi:///

# The LDAP protocol version to use.
ldap_version 3

# The search base that will be used for all queries.
base ou=People,dc=example,dc=tld


# SSL options
ssl off
tls_reqcert allow

# The search scope.
scope sub

log none
# search into authorizedService to allow authentication. (ssh, openvpn or whatever service that can authenticate against ldap)
pam_authz_search (&(uid=$username)(authorizedService=$service))
sasl_mech EXTERNAL

# Custom search filters
filter passwd (!(uid=root))
filter shadow (!(uid=root))

```

* If you have custom olcAcces rules maybe you need to add the following  at the beginning of your config file (something like olcDatabase\=\{1\}mdb.ldif:

```
olcAccess: {0}to attrs=userPassword,shadowLastChange by self write by anonymous auth by * none
olcAccess: {1}to dn.base="" by * read
olcAccess: {2}to * by * read

```  
### nsswitch 

Edit /etc/nsswitch.conf 

```
passwd:     compat ldap
shadow:     compat ldap
gshadow:    files
group:      compat ldap
hosts:      files dns 
ethers:     db files
networks:   files
protocols:  db files
rpc:        db files
services:   db files
netgroup:   nis 

```

## pam configuration  

Check that the oad_ldap.so is included in following files:

/etc/pam.d/common-session
```
session [success=ok default=ignore] pam_ldap.so minimum_uid=1000
```

/etc/pam.d/common-auth 
```
auth    [success=1 default=ignore]      pam_ldap.so minimum_uid=1000 use_first_pass
```

/etc/pam.d/common-password 
```
password        [success=1 default=ignore]      pam_ldap.so minimum_uid=1000 try_first_pass
``` 

/etc/pam.d/common-account
```
account [success=ok new_authtok_reqd=done ignore=ignore user_unknown=ignore authinfo_unavail=ignore default=bad]            pam_ldap.so minimum_uid=1000
```

/etc/pam.d/common-session-noninteractive
```
session [success=ok default=ignore]     pam_ldap.so minimum_uid=1000
```



