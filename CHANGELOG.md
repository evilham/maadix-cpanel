# Maadix Control Panel Changelog

## [202101]

### Added
- Vms connection to new API
- FQDN validation
- New forms for application installation with password field (avoiding emailing)
- Trash for system generated backup files from previous versions. 
- Form in the server activation process to set mysql password and not to be sent by email.

### Changed
- Encryption of application passwords with root public key so they are not stored in plain text on the system.

### Fixed
- The applications installation panel layout.
- Menus to read new API links

## [201902]

### Added

- Report submission button prior to Buster upgrade.
- Error/success message after report submission.
- System status check before performing Buster upgrade.
- Disk space check before upgrading to Buster.

### Fixed
- Translation and typos
