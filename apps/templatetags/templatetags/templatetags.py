# python
import os, socket
# django
from django import template
from django.utils.translation import ugettext_lazy as _, get_language
from django.utils.safestring import mark_safe
# project
from django.conf import settings
from apps.views import utils

register = template.Library()

@register.simple_tag
def css(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/css/' + file

@register.simple_tag
def js(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/js/' + file

@register.simple_tag
def img(file):
    return  settings.STATIC_URL + settings.PROJECT_STATIC_FOLDER + '/img/' + file

@register.filter
def order_by(queryset, args):
    args = [x.strip() for x in args.split(',')]
    return queryset.order_by(*args)

@register.filter
def verbose_name(obj):
    return obj._meta.verbose_name

@register.filter
def contenttype(obj):
    return obj.__class__.__name__.lower()

@register.filter
def verbose_name_slug(obj):
    return slugify(obj._meta.verbose_name)

@register.filter
def sort_by(queryset, order):
    return queryset.order_by(order)

@register.filter
def domain_status(domain):
    return os.path.isfile('/etc/apache2/ldap-enabled/%s.conf' % domain)

@register.simple_tag
def get_server_host():
    return utils.get_server_host()

@register.simple_tag
def get_server_ip():
    return utils.get_server_ip()

@register.filter
def split_domain(domain):
    return domain.split('.')

@register.filter()
def clean_description(description):
    return utils.clean_api_description(description)

@register.simple_tag
def asset_img(url):
    return  settings.ASSETS_URI + settings.IMAGES_FOLDER + url

@register.filter('startswith')
def startswith(text, starts):
    if isinstance(text, str):
        return text.startswith(starts)
    return False

@register.inclusion_tag('blocks/default-form.html')
def default_form(form=None, modifier=None, submit_text=None):
    return {
        'form' : form,
        'modifier' : modifier,
        'submit_text' : submit_text
    }

@register.simple_tag
def fake_breadcrumb(text=_("Volver atrás")):
    markup = "<button class='fake-breadcrumb' onclick='history.go(-1)'>%s</button>" % text
    return mark_safe(markup)

@register.filter
def split(txt, sep):
    return txt.split(sep)

@register.filter
def switch(options, option):
    return options.get(option, options['default'])

@register.filter
def mbs(value):
    return value / (1024 * 1024)

@register.filter
def gbs(value):
    return value / (1024 * 1024 * 1024)

@register.filter
def sub(total, val):
    return total-val

@register.simple_tag
def range(max):
    return [max]

@register.simple_tag(takes_context = True)
def cookie(context, cookie_name): # could feed in additional argument to use as default value
    request = context['request']
    result = request.COOKIES.get(cookie_name,'') # I use blank as default value
    print (result)
    return result
    
@register.filter
def get_countitem(myform, key):
    for field in myform:
        if field.name == key:
            return field
    return None

@register.simple_tag
def get_appfield(form, prefix, service_id, field_id):
    fieldname = '%s-%s-%s' % ( prefix, service_id, field_id )
    for field in form:
        if field.name == fieldname:
            return field
    return None

@register.filter
def fqdn(url):
    if url.startswith('http') or url.startswith('/'):
        return url
    return '//%s' % url
