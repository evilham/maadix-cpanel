# python
import grp, math, json, os
from datetime import datetime, timedelta
# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse, resolve
from django.contrib import messages
from django.utils.html import escape
from django.contrib.auth.views import LoginView as login
from django.contrib.auth.views import LogoutView as logout
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.contrib.auth import logout as auth_logout
from django.core.signing import TimestampSigner
# contrib
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from ldap3.utils.hashed import hashed
from ldap3.core.exceptions import LDAPSocketOpenError
# project
from django.conf import settings
from . import utils, forms

def LoginRedirect(request):
    # Custom landing url to redirect users after login according to their roleº
    # With 2FA the LoginView is provided by two_factor
    user = request.user.username
    """ Redirection url if form is valid. Depends on user's role """
    role = utils.get_user_role(user)
    if role == 'postmaster':
        return HttpResponseRedirect(reverse('mails'))
    elif role == 'email':
        return HttpResponseRedirect(reverse('email')+ "?mail=personal" )
    else:
        return HttpResponseRedirect(reverse('system-details'))

class LogoutView(logout):
    """ Custom logout view that destroys session data """

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        next_page = self.get_next_page()
        # clean session data
        response  = HttpResponseRedirect(next_page)
        # username = request.session['user']
        # del request.session['user']
        # del request.session['enc_pass']
        # Destroy cookies
        # response.delete_cookie('s')
        # redirect to settings.LOGOUT_REDIRECT_URL
        auth_logout(request)
        return response

class Profile(FormView):
    """ User profile view """

    form_class    = forms.ProfileForm
    template_name = 'pages/profile.html'

    def get_success_url(self, pwd):
        """Returns the URL to redirect user after a succesful submit"""
        # logout if password was updated
        if pwd:
            return reverse('logout')
        return self.request.get_full_path()

    def get_form(self):
        """ Return an instance of the form to be used in this view."""
        username = self.request.user.username
        self.role     = utils.get_user_role(username)
        if self.role == 'admin':
            userfilter = "(&(objectClass=extensibleObject)(cn=%s))" % username
        else:
            userfilter = "(mail=%s)" % username
        self.request.ldap.search(
            settings.LDAP_TREE_BASE,
            userfilter,
            attributes=['email']
        )
        user  = self.request.ldap.entries[0]
        kwargs = self.get_form_kwargs()
        kwargs['initial'] = {
            'username' : username,
            'email'    : user.email.value,
        }
        return forms.ProfileForm(
            #pwd  = utils.dec_pwd(self.request),
            role = self.role,
            **kwargs
        )

    def form_valid(self, form):
        """ Actions to be performed if the form is valid. """
        username = self.request.user.username
        pwd      = ''
        old_pswd = form['current_password'].value()
        # set fields to be updated
        fields = {}
        if 'email' in form.fields and form['email'].value():
            fields['email'] = [(MODIFY_REPLACE, form['email'].value())]
        if 'password' in form.fields and form['password'].value():
            pwd = form['password'].value()
            fields['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, pwd))]
        # build dn of the current user
        if self.role == 'admin':
            dn     = "cn=%s,%s" % (username, settings.LDAP_TREE_BASE)
        elif self.role == 'postmaster':
            domain = username.split('@')[1]
            dn     = "cn=postmaster,vd=%s,%s" % (domain, settings.LDAP_TREE_HOSTING)
        else:
            domain = username.split('@')[1]
            dn     = "mail=%s,vd=%s,%s" % (username, domain, settings.LDAP_TREE_HOSTING)
        # modify user fields
        # Use password provided by user to connect and bind to ldap
        try:
            ldap = utils.connect_ldap(dn, old_pswd)
            if ldap['connection']:
                bind = ldap['connection']
                bind.modify(dn, fields)
                if pwd:
                    messages.success(self.request, _('Has cambiado tu perfil con éxito. '
                                             'Utiliza la nueva contraseña para entrar.'))
                else:
                    messages.success(self.request, _('Has cambiado tu perfil con éxito.'))
            # Update User Model to have same data as in LDAP
            else:
                messages.error(self.request, _('La contraseña antigua no es correcta' ))
        except Exception as e:    
                messages.error(self.request, _('La contraseña antigua no es correcta ' ))
                utils.p("✕ view_users.py", "There's a problem in  LDAP: ", e)
        return HttpResponseRedirect(self.get_success_url(pwd))

class Users(FormView):
    """ Email accounts list view and form to add a new email account"""

    template_name = 'pages/users.html'
    form_class    = forms.UserForm
    ldap          = {}
    emails        = []
    search_filter = settings.LDAP_FILTER_ALL_USERS
    #home_path     = "/home/sftpusers"
    #gidnumber     = grp.getgrnam('sftpusers')[2]
    #jailed_user   = True
    #usertype     = 'none'
    # home directory is a required field. Must be a string
    #home_directory= 'none' 
    jailed_user   = False
    success_url   = reverse_lazy('users')

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        emails = utils.get_existing_emails(self.request.ldap)
        self.emails = [ email.mail for email in emails ]
        return self.form_class(
            usertype='none',
            emails=self.emails,
            services=self.request.enabled_services,
            edit_form=False,
            #ldap=self.request.ldap,
            **self.get_form_kwargs()
        )

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        users = []
        # Create filters to query users by suthorized service
        user_services = ['all','ssh', 'sftp']
        enabled_services=self.request.enabled_services
        self.ser_filter = self.request.GET.get('service')
        for user_serv in settings.USER_SERVICES:
            if user_serv in enabled_services:
                user_services.append(user_serv)
        if self.ser_filter and self.ser_filter in enabled_services:
            # Exception for phpmyadmin, as the authorized service is apache
            if self.ser_filter == 'phpmyadmin':
                self.search_filter = "(&(objectClass=person)(uid=*)(authorizedService=apache)(!(gidnumber=27)))"
            else:
                self.search_filter = "(&(objectClass=person)(uid=*)(authorizedService=%s)(!(gidnumber=27)))" % self.ser_filter
        elif self.ser_filter and self.ser_filter=='ssh':
            self.search_filter = settings.LDAP_FILTERS_SSH
        elif self.ser_filter and self.ser_filter=='sftp':
            self.search_filter = settings.LDAP_FILTERS_SFTP
        try:
            # Get users and enabled services for each one
            self.request.ldap.search(
                search_base   = settings.LDAP_TREE_USERS,
                search_filter = self.search_filter,
                attributes    = ['uid','authorizedservice','userpassword', 'gidnumber', 'homedirectory']
            )
            for user in self.request.ldap.entries:
                authorized_services = user.authorizedservice.value
                # We set the user by default as no jailed. If then sftp access is grantes users will be added
                # To the sftp usres gorup. It won't be impossible then to swith this user to ssh, at least at the moment
                # as it would mean moving their home. Once a user has been granted sshd service their home is created.
                # So form now users can not be transfirmed from sftp to ssh and vice versa 
                if user.homedirectory.value=='none':
                    self.usertype='none'
                else:
                    if int(user.gidnumber.value)==int(settings.SFTP_GUID):
                        self.usertype='sftp'
                    else:
                        self.usertype='ssh'
                users.append({
                    'username'  : user.uid.value,
                    'dn'        : user.entry_dn,
                    'sftp'      : utils.check_value('sshd',    authorized_services),
                    'apache'    : utils.check_value('apache',  authorized_services),
                    'openvpn'   : utils.check_value('openvpn', authorized_services),
                    'jitsi'     : utils.check_value('jitsi', authorized_services),
                    'usertype'  : self.usertype,
                })
            users = sorted(users, key = lambda i: i.get('username'))
            # Check if users are webmasters and domains maintained by them
            # We iterate twice because ldap entries are overwritten in every call to search()
            for user in users:
                try:
                    self.request.ldap.search(
                        search_base   = settings.LDAP_TREE_BASE,
                        search_filter = "(&(vd=*)(adminid=%s))" % user['username'],
                        attributes    = ['vd']
                    )
                    if self.request.ldap.entries:
                        user['domains'] = [ domain.vd for domain in self.request.ldap.entries ]
                except Exception as e:
                    utils.p("✕ view_users.py", "There's a problem fetching users from LDAP: ", e)
        except Exception as e:
            utils.p("✕ view_users.py", "There's a problem fetching users from LDAP: ", e)
        # Check if domains module is in error status
        # This module must run when a user is deleted, tomove its home intos tre .trash folder
        domains_module = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        context = super(Users, self).get_context_data(**kwargs)
        context['domains_module'] = domains_module
        context['users']        = users
        context['phpmyadmin']   = True if 'phpmyadmin' in enabled_services else False
        context['vpn']          = True if 'openvpn' in enabled_services else False
        context['jitsi']        = True if 'jitsi' in enabled_services else False
        context['display_form'] = 'form' in kwargs
        context['user_services'] = user_services
        return context

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        user           = form['username'].value().strip()
        name           = form['name'].value().strip()
        surname        = form['surname'].value().strip()
        email          = form['email'].value().strip()
        uidnumber      = None
        password       = form['password'].value()
        sshkey         = form['sshkey'].value().strip()
        services       = []

        for service in ['openvpn', 'apache', 'jitsi']:
            if service in form.fields and form[service].value():
                services.append(service)
        self.request.ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = "(&(objectClass=uidNext)(uidnumber=*))",
            attributes    = ['uidnumber']
        )
        next_uid = int(self.request.ldap.entries[0].uidnumber.value) + 1
        if next_uid:
            uidnumber = next_uid
            try:
                dn = 'cn=uidNext,ou=sshd,ou=People,dc=example,dc=tld'
                self.request.ldap.modify(dn, { 'uidnumber' : [(MODIFY_REPLACE, next_uid)] })
            except Exception as e:
                utils.p("✕ view_users.py", "There was a problem updating next uid value: ", e)
        if form['sshSftp'].value() and not form['sshSftp'].value()=="none":
            if form['sshSftp'].value()=='ssh':
                home_path     = "/home"
                jailed_user   = False
                gidnumber     = next_uid 
                usertype      = 'ssh'
            elif form['sshSftp'].value()=='sftp':
                home_path     = "/home/sftpusers"
                gidnumber     = grp.getgrnam('sftpusers')[2]
                jailed_user   = True
                usertype     = 'sftp'
            services.append('sshd')
            home_directory = "%s/%s" % (home_path, user)
        else:
            gidnumber     = next_uid
            home_directory= 'none'
        login_shell    = "/bin/bash"

        dn = 'uid=%s,%s' % (user, settings.LDAP_TREE_USERS)
        attributes = {
                'uid'               : user,
                'uidnumber'         : uidnumber,
                'gidnumber'         : gidnumber,
                'gecos'             : '%s,,,' % user,
                'cn'                : name if name else user,
                'sn'                : surname if surname else user,
                'mail'              : email,
                'loginshell'        : login_shell,
                'homedirectory'     : home_directory,
                'shadowlastchange'  : math.floor(utils.unix_timestamp() / 86400),
                'shadowmax'         : '99999',
                'shadowwarning'     : '7',
                'userpassword'      : hashed(HASHED_SALTED_SHA, password),
                'sshpublickey'      : sshkey,
            }

        if services:
            attributes['authorizedservice'] = services
        try:
            self.request.ldap.add(dn, [
                'person',
                'organizationalPerson',
                'inetOrgPerson',
                'posixAccount',
                'top',
                'shadowAccount',
                'authorizedServiceObject',
                'ldapPublicKey',
            ], attributes 
            )
            if 'openvpn' in self.request.enabled_services and form['instructions'].value():
                utils.send_vpn_instructions(self.request, email, user)
            ### Add the user to the web group, if ssh is enabled, for sftp users
            # this will make the files uploaded by these users being owned by useename:web
            # web is a group which includes www-data. So apache will be able to access folders
            if form['sshSftp'].value()=='sftp':
                try:
                    dn = 'cn=web,ou=groups,ou=People,%s' % (settings.LDAP_TREE_BASE)
                    self.request.ldap.modify(dn, {
                        'memberuid'         : [(MODIFY_ADD, user)],
                    })
                except Exception as e:
                    utils.p("✕ view_users.py", "There was a problem creating the user %s :" % user, e)

            messages.success(self.request, _('Cuenta añadida con éxito'))
            utils.p("✔ view_users.py", "Usuarix %s añadid- con éxito':" % user, '')
        except Exception as e:
            messages.error(self.request, _('Se ha producido un error creando la cuenta %s' % user))
            utils.p("✕ view_users.py", "There was a problem creating the user %s :" % user, e)
        
        return super().form_valid(form)

    def post(self, request):
        form = self.get_form()
        if request.method=='POST' and 'lockdomains' in request.POST:
            try:
                utils.lock_cpanel_local(self.request.ldap, 'domains');
                messages.success(self.request, _('Reanudando operación previa. El proceso tardará unos minutos'))
            except Exception as e:
                print(e)
            return HttpResponseRedirect(self.success_url)
        else:
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)

class User(FormView):
    """ Form to edit a user account"""

    template_name = 'pages/user.html'
    form_class    = forms.UserForm
    #home_path     = "none"
    jailed_user   = False

    def get_success_url(self):
        return reverse('users')

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        username = self.request.GET.get('username')
        emails = utils.get_existing_emails(self.request.ldap)
        self.emails  = [ email.mail for email in emails ]
        self.domains = utils.get_user_domains(self.request.ldap, username)
        #Check if is a sftp or ssh user
        #home_path     = "/home"
        #jailed_user   = False
        try:
            self.request.ldap.search(
                search_base   = settings.LDAP_TREE_BASE,
                search_filter = "(uid=%s)" % username,
                attributes = [
                    'uid',
                    'uidnumber',
                    'gidnumber',
                    'gecos',
                    'cn',
                    'sn',
                    'mail',
                    'gidnumber',
                    'loginshell',
                    'homedirectory',
                    'shadowlastchange',
                    'shadowmax',
                    'shadowwarning',
                    'authorizedservice',
                    'sshpublickey',
                ]
            )
            self.user = self.request.ldap.entries[0]
            # Check if user has already be granted ssh or sftp access.
            # This is done trhough the homedir and the group it belongs. 
            # Need this value to build the form as a sftp user can not be sitched to ssh
            if self.user.homedirectory.value=='none':
                self.usertype='none'
            else:
                if int(self.user.gidnumber.value)==int(settings.SFTP_GUID):
                    self.usertype='sftp'
                else:
                    self.usertype='ssh'
        
            kwargs    = self.get_form_kwargs()
            services  = self.user.authorizedservice
            kwargs['initial'] = {
                'username' : self.user.uid.value,
                'name'     : self.user.cn.value,
                'surname'  : self.user.sn.value,
                'email'    : self.user.mail.value,
                'sshkey'   : self.user.sshpublickey.value,
                'home_dir' : self.user.homedirectory.value,
                'sshd'     : 'sshd' in services,
                'openvpn'  : 'openvpn' in services,
                'apache'   : 'apache' in services,
                'jitsi'    : 'jitsi' in services

            }
            return self.form_class(
                emails=self.emails,
                domains=self.domains,
                usertype=self.usertype,
                services=self.request.enabled_services,
                edit_form=True,
                **kwargs
            )
        except Exception as e:
            print("There was a problem retrieving the user: ")
            print(str(e))
            return HttpResponseRedirect(self.get_success_url())
        return HttpResponseRedirect(self.get_success_url())

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        user           = form['username'].value().strip()
        name           = form['name'].value().strip()
        surname        = form['surname'].value().strip()
        email          = form['email'].value().strip()
        sshkey         = form['sshkey'].value().strip()
        password       = form['password'].value()
        services       = []
            
        for service in ['openvpn', 'apache', 'sshd', 'jitsi']:
            if service in form.fields and form[service].value():
                services.append(service)
        dn = 'uid=%s,%s' % (user, settings.LDAP_TREE_USERS)
        attr = {
                'cn'                : [(MODIFY_REPLACE, name if name else user)],
                'sn'                : [(MODIFY_REPLACE, surname if surname else user)],
                'mail'              : [(MODIFY_REPLACE, email)],
                'shadowlastchange'  : [(MODIFY_REPLACE, math.floor(utils.unix_timestamp() / 86400))],
		'sshpublickey'	    : [(MODIFY_REPLACE, sshkey.strip())],
            }
        # If a user had no previous ssh/sftp access the sshSftp form field 
        # will be diplayed instead of sshd field. WE get its value to check 
        # If sshd acces has been activated and if so, check if it is sfto or ssh
        # and change homedir and group according to this value
        if  self.usertype == 'none':
            if form['sshSftp'].value() and not form['sshSftp'].value()=="none":
                if form['sshSftp'].value()=='ssh':
                    home_path     = "/home"
                    jailed_user   = False
                    self.usertype      = 'ssh'
                elif form['sshSftp'].value()=='sftp':
                    home_path     = "/home/sftpusers"
                    self.jailed_user   = True
                    self.usertype     = 'sftp'
                    attr['gidnumber'] = [(MODIFY_REPLACE, settings.SFTP_GUID)]
                services.append('sshd')
                home_directory = "%s/%s" % (home_path, user)
                attr['homedirectory'] = [(MODIFY_REPLACE, home_directory)]
        attr['authorizedservice'] = [(MODIFY_REPLACE, services)]
        # Check if objectClass ldapPublicKey exists for user,
        #in order to be able to add sshPublicKey attribute
        #if sshkey:
        try:
            self.request.ldap.search(
                dn,
                search_filter = '(objectClass=ldapPublicKey)',
                attributes    = [ 'uid' ]
                )  
            existkeyObject = self.request.ldap.entries[0]
        except Exception as e:
            #self.request.ldap.modify(dn, ['ldapPublicKey'],{'sshPublicKey':sshkey})
            attr['objectclass'] = [(MODIFY_ADD,'ldapPublicKey')]
            # Update sshkey value
        attr['sshpublickey'] = [(MODIFY_REPLACE, sshkey.strip())]
        # Only update password if it has been changed
        if password:
            attr['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, password))]
        # Update user group if sshd is activated. It must be the sftpusers guidnumber
        """
        if 'sshd' in services:
            home_directory = "%s/%s" % (self.home_path, user)
            login_shell    = "/bin/bash"
            #gidnumber     = grp.getgrnam('sftpusers')[2] if self.jailed_user  else self.user.uidnumber.value
            attr['homedirectory'] = [(MODIFY_REPLACE, home_directory)]
            attr['loginshell'] = [(MODIFY_REPLACE, login_shell)]
            #attr['gidnumber'] = [(MODIFY_REPLACE, gidnumber)]
        """
        try:
            self.request.ldap.modify(dn, attr)
            # Update web group depending on sshd service setting
            # cn=web,ou=groups,ou=People 
            # First check if user exists in web group
            dn = "cn=web,ou=groups,ou=People,%s" % settings.LDAP_TREE_BASE
            try:
                self.request.ldap.search(
                    search_base   = dn, 
                    search_filter = "(&(memberuid=%s))" % user,
                    attributes    = ['memberuid']
               )
                web_user = self.request.ldap.entries
            
            except Exception as e:
                utils.p("✕ view_users.py", "There was a problem updating the user %s :" % user, e)
            # if user has not sshd and exists in web group, remove from froup
            # Only sftp users can belong to web group
            if self.jailed_user:
                if not 'sshd' in services and not user in web_user:
                    attr = {'memberuid' : [(MODIFY_DELETE, user)]}
                # if user has sshd but is not in web group, add to the group
                elif 'sshd' in services and not web_user:
                    attr = {'memberuid' : [(MODIFY_ADD, user)]}
                try:
                    self.request.ldap.modify(dn, attr)    
                except Exception as e:
                    utils.p("✕ view_users.py", "There was a problem updating the web group for  user %s :" % user, e)

            messages.success(self.request, _('Cuenta modificada con éxito'))
            if 'openvpn' in services:
                try:
                    if form['instructions'].value():
                        utils.send_vpn_instructions(self.request, email, user)
                        messages.success(self.request, _('Instrucciones enviadas con éxito'))
                except Exception as e:
                     messages.error(self.request, _('Se ha producido un fallo en el envío de las instrucciones para la conexión VPN'))
            utils.p("✔ view_users.py", "Usuari- %s modificdo - con éxito':" % user, '')
        except Exception as e:
            messages.error(self.request, _('Ha habido un error modificando la cuenta. '
                                           'Los cambios no se han guardado. Disculpa las '
                                           'molestias.'))
            utils.p("✕ view_users.py", "There was a problem updating the user %s :" % user, e)

        return super().form_valid(form)


    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context  = super().get_context_data(**kwargs)
        username = self.request.GET.get('username')
        context['username'] = username

        return context

class SuperUser(FormView):
    """
    Form to edit the superuser account.
    """

    template_name = 'pages/superuser.html'
    form_class    = forms.SuperuserForm

    def get_success_url(self):
        return self.request.get_full_path()

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        # get existing emails
        emails = utils.get_existing_emails(self.request.ldap)
        self.emails = [ email.mail for email in emails ]
        try:
            self.request.ldap.search(
                search_base   = settings.LDAP_TREE_BASE,
                search_filter = settings.LDAP_FILTERS_SUPERUSER,
                attributes = [
                    'uid',
                    'cn',
                    'sn',
                    'mail',
                    'homedirectory',
                    'authorizedservice',
                    'sshpublickey',
                ]
            )

            self.user = self.request.ldap.entries[0]
            kwargs    = self.get_form_kwargs()
            services  = self.user.authorizedservice
            kwargs['initial'] = {
                'username' : self.user.uid.value,
                'name'     : self.user.cn.value,
                'surname'  : self.user.sn.value,
                'email'    : self.user.mail.value,
                'sshkey'   : self.user.sshpublickey.value,
                'openvpn'  : 'openvpn' in services,
                'apache'   : 'apache' in services,
                'jitsi': 'jitsi' in services,
                'home_dir' : self.user.homedirectory.value,
            }
            return forms.SuperuserForm(
                emails   = self.emails,
                services = self.request.enabled_services,
                **kwargs
            )
        except Exception as e:
            print("There was a problem retrieving the user: ")
            print(str(e))
        return forms.SuperuserForm(
            emails   = self.emails,
            services = self.request.enabled_services,
            **self.get_form_kwargs()
        )
    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        user           = self.user
        username       = self.user.uid.value
        name           = form['name'].value().strip()
        surname        = form['surname'].value().strip()
        email          = form['email'].value().strip()
        sshkey         = form['sshkey'].value().strip()
        password       = form['password'].value()
        current_password= form['current_password'].value()
        # Always give superuser cron and sshd autorization
        services       = ['cron', 'sshd', 'sudo']
        for service in ['openvpn', 'apache', 'jitsi']:
            if service in form.fields and form[service].value():
                services.append(service)
        dn = 'uid=%s,%s' % (username, settings.LDAP_TREE_USERS)
        attr = {
                'cn'                : [(MODIFY_REPLACE, name if name else username)],
                'sn'                : [(MODIFY_REPLACE, surname if surname else '')],
                'mail'              : [(MODIFY_REPLACE, email if email else '')],
                'shadowlastchange'  : [(MODIFY_REPLACE, math.floor(utils.unix_timestamp() / 86400))],
                'authorizedservice' : [(MODIFY_REPLACE, services)],

             #   'userpassword'      : [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, password))],
            }
        # Check if objectClass ldapPublicKey exists for user,
        #in order to be able to add sshPublicKey attribute
        #if sshkey:
        try:
            self.request.ldap.search(
                dn, 
                search_filter = '(objectClass=ldapPublicKey)',
                attributes    = [ 'uid' ]
                )   
            existkeyObject = self.request.ldap.entries[0]
        except Exception as e:
            #self.request.ldap.modify(dn, ['ldapPublicKey'],{'sshPublicKey':sshkey})
            attr['objectclass'] = [(MODIFY_ADD,'ldapPublicKey')]
            # Update sshkey value
        attr['sshpublickey'] = [(MODIFY_REPLACE, sshkey.strip())]
        if password:
            attr['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, password))]
        # Use session name and password provided by user to bind and write in ldap
        admin =  self.request.user.username
        cn = 'cn=%s,%s' % (admin, settings.LDAP_TREE_BASE)
        ldapown = utils.connect_ldap(cn, current_password)
        bind = ldapown['connection']
        if ldapown['connection']:
            try:
                bind.modify(dn, attr)
                if 'openvpn' in form.fields and form['instructions'].value():
                    utils.send_vpn_instructions(self.request, email, username)
                messages.success(self.request, _('Supersuarix modificado con éxito'))
            except Exception as e:
                messages.error(self.request, _('Ha habido un error modificando la cuenta. '
                                               'Los cambios no se han guardado.'))
                utils.p("✕ view_users.py", "There was a problem updating superuser %s :" % user, e)
        else:
            messages.error(self.request, _('La contraseña de acceso al panel de control no es correcta.'))

        return super(SuperUser, self).form_valid(form)

class Postmasters(views.View):
    """
    Postmasters list view.
    """
    def get(self, request):
        active_postmasters = []
        # get domains in ldap 
        self.request.ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = '(vd=*)',
            attributes    = [ 'vd' ],
        )
        postmasters = { entry.vd.value : 0 for entry in self.request.ldap.entries }
        # Create list with domains, to look for created postmasters
        existing_domains = [ entry.vd.value  for entry in self.request.ldap.entries ] 
        for domain in existing_domains:
            try:
                self.request.ldap.search(
                    search_base   = 'cn=postmaster,vd=%s,%s' % (domain, settings.LDAP_TREE_HOSTING),
                    search_filter = '(objectClass=VirtualMailAlias)',
                    attributes    = [ 'cn' ]
                )
                postmaster = self.request.ldap.entries
            except:
                postmaster = False
            if postmaster:
                active_postmasters.append(domain)

        # get emails
        self.request.ldap.search(
            search_base   = 'o=hosting,dc=example,dc=tld',
            search_filter = '(objectClass=VirtualMailAccount)',
            attributes    = ['mail', ]
        )
        # update number of emails related to each postmaster
        for entry in self.request.ldap.entries:
            domain = entry.mail.value.split('@')[1]
            if domain in postmasters:
                postmasters[domain] += 1

        return render(request, 'pages/postmasters.html', {
            'postmasters' : postmasters, 'active_postmasters' : active_postmasters 
        })

class Postmaster(FormView):
    """
    Form to edit users with the role postmaster.
    """

    template_name = 'pages/postmaster.html'
    form_class    = forms.PostmasterForm

    def get_success_url(self):
        return reverse('postmasters')

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        self.domain = self.request.GET.get('domain')
        kwargs = self.get_form_kwargs()
        kwargs['initial']['username'] = 'postmaster@%s' % self.domain
        return forms.PostmasterForm(**kwargs)

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""

        context  = super(Postmaster, self).get_context_data(**kwargs)
        #domain = self.request.GET.get('domain')
        context['domain'] = self.domain
        return context

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        password = hashed(HASHED_SALTED_SHA, form['password'].value())

        dn = 'cn=postmaster,vd=%s,%s' % (self.domain, settings.LDAP_TREE_HOSTING)
        # Check if postmaster exists fro current domain
        try:
            self.request.ldap.search(
                search_base   = dn,
                search_filter = '(objectClass=VirtualMailAlias)',
                attributes    = [ 'cn' ]
            )
            postmaster = self.request.ldap.entries
        except:
            postmaster = False
        # If postmaster exists, edit it, else create entry 
        if postmaster: 
            try:
                self.request.ldap.modify(dn, {
                    'userpassword'      : [(MODIFY_REPLACE, password)],
                })
                messages.success(self.request, _('Contraseña modificada con éxito'))
            except Exception as e:
                messages.error(self.request, _('Ha habido un error modificando el postmaster. '
                                               'Los cambios no se han guardado. Disculpa las '
                                               'molestias. Si el problema persiste contacta '
                                               'con los administrador-s'))
                utils.p("✕ view_users.py", "There was a problem updating webmaster" , e)
        else:
            try:
                self.request.ldap.add(dn, ['VirtualMailAlias', 'top'], {
                    'cn'            : 'Postmaster',
                    'sn'            : 'Postmaster',
                    'mail'          : 'postmaster@%s' % self.domain,
                    'maildrop'      : 'postmaster',
                    'accountActive' : 'TRUE',
                    'creationDate'  : utils.ldap_creation_date(),
                    'lastChange'    : utils.unix_timestamp(),
                    'userpassword'  : password
                })
                messages.success(self.request, _('Cuenta de postmaster activada con éxito'))
            except Exception as e:
                messages.error(self.request, _('Ha habido un error activando el postmaster. '))
                utils.p("✕ view_users.py", "There was a problem creating webmaster" , e) 

        return super(Postmaster, self).form_valid(form)
