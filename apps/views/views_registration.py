from django.contrib.auth import (
    REDIRECT_FIELD_NAME, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
)
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect, QueryDict
from django.shortcuts import resolve_url
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django_auth_ldap.backend import LDAPBackend
# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse, resolve
from django.contrib import messages
from django.utils.html import escape
from django.contrib.auth.views import LoginView as login
from django.contrib.auth.views import LogoutView as logout
from django.contrib.auth.views import PasswordResetView as passreset
from django.contrib.auth.views import PasswordResetConfirmView as passconfirm,PasswordResetDoneView as passresetdone
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import get_user_model
from django.core.signing import TimestampSigner
# contrib
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from ldap3.utils.hashed import hashed
from ldap3.core.exceptions import LDAPSocketOpenError
# project
from django.conf import settings
from . import utils, forms

UserModel = get_user_model()
class PasswordResetView(passreset):
    email_template_name = 'emails/password_reset_email.html'
    extra_email_context = None
    #form_class = PasswordResetForm
    form_class= forms.PassResetForm
    from_email = None
    html_email_template_name = None
    subject_template_name = 'emails/password_reset_subject.txt'
    success_url = reverse_lazy('pass-recover-done')
    template_name = 'registration/password-recovery.html'
    title = _('Recuperar contraseña')
    token_generator = default_token_generator
    """
    def get_form(self):
        #Return an instance of the form to be used in this view.
        ldap_admin = utils.get_admin(self.request.ldap)
        self.admin_email =  ldap_admin.email
        return self.form_class(
            **self.get_form_kwargs()
        )
    """
    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': self.from_email,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': self.extra_email_context,
        }
	# Email is sent twice
        #form.save(**opts)
        return super().form_valid(form)


INTERNAL_RESET_URL_TOKEN = 'set-password'
INTERNAL_RESET_SESSION_TOKEN = '_password_reset_token'
#INTERNAL_RESET_SESSION_TOKEN='session_token'
""" USE custom template """
class PasswordResetDoneView(passresetdone):
    template_name = 'registration/password-resetconfirm.html'
    title = _('Password reset sent')


class PasswordResetConfirmView(passconfirm):
    template_name = 'registration/password-reset-new.html'
    success_url = reverse_lazy('password-reset-success')
    post_reset_login = True 
    """ Add Ldap update for passowrd """

    def form_valid(self, form):
        user = form.save()
        if self.post_reset_login:
            auth_login(self.request, user, self.post_reset_login_backend)
        ldapanon  = utils.anonymous_connect_ldap()
        ldap_admin = utils.get_admin(ldapanon)
        # Update password in ldap is user in session is same as in ldap
        if ldap_admin.cn==user.username: 
            fields = {}
            pwd = form['new_password1'].value()
            fields['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, pwd))]
            try:
                ldapConn     = utils.connect_ldap(
                    settings.AUTH_LDAP_BIND_DN, 
                    settings.AUTH_LDAP_BIND_PASSWORD
                )   
                ldap = ldapConn['connection']
                ldap.modify(ldap_admin.entry_dn, fields)
            except Exception as e:
                messages.error(self.request, _('Se ha procudico un error. Vuelve aintentarlo' ))
                utils.p("✕ view_registration.py", "There's a problem writing password in  LDAP: ", e)
        else:
            messages.error(self.request, _('Se ha procudico un error. Vuelve a intentarlo' ))
        return super().form_valid(form)

class PasswordResetSucessView(views.View):

    def get(self,request):
        #Log out user. Django create session after pass recover
        auth_logout(self.request)
        return render(request, 'registration/password-reset-done.html')

