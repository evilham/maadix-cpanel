# python
from re import compile
from socket import error as socket_error
# django
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import PermissionDenied
from django.utils.text import slugify
# contrib
from ldap3.core.exceptions import LDAPSocketOpenError, LDAPNoSuchObjectResult
# project
from django.conf import settings
from . import utils

class LdapConnectionMiddleware:
    """
    Middleware that requires a user to be authenticated to view any page other
    than LOGIN_URL. Exemptions to this requirement can optionally be specified
    in settings via a list of regular expressions in LOGIN_EXEMPT_URLS (which
    you can copy from your urls.py).
    """

    def __init__(self, get_response):
        self.get_response = get_response
        # Create list of urls that are allowed for the different roles
        # [we compile urls as regex expressions to allow url patterns in ALLOWED_URLS if needed]
        self.ALLOWED_URLS = { k:[compile(e) for e in v] for k,v in settings.ALLOWED_URLS.items() }

    def __call__(self, request):
        
        # get current path
        path = request.path_info.lstrip('/')
        login_url = reverse( 'two_factor:login' )
        restricted_view = not any(regex.match(path) for regex in self.ALLOWED_URLS['anonymous']) 
        
        #if restricted_view and request.user.is_anonymous():
        if restricted_view and str(request.user)=='AnonymousUser':
            return HttpResponseRedirect( login_url )
        elif request.user.is_authenticated: 
            try:
                # Connect ldap
                role     = utils.get_user_role(request.user.username)
                ldap     = utils.connect_ldap(
                    settings.AUTH_LDAP_BIND_DN, 
                    settings.AUTH_LDAP_BIND_PASSWORD
                )
                # if connection, check permissions and set ldap as a property of
                # the request, available to any view
                if ldap['connection']:
                    # Check permissions
                    if not any(regex.match(path) for regex in self.ALLOWED_URLS[role]):
                        if role == 'anonymous':
                            messages.success(request, _('Has de estar logueado para ver esa página'))
                            return HttpResponseRedirect( login_url )
                        raise PermissionDenied
                    # set session parameters as request attributes
                    request.user.role     = role
                    setattr(request, "ldap", ldap['connection'])

                    # If role is admin get ldap status
                    # Part of this status will be passed to global context (menu, notifications)
                    # through context_processors.py
                    if role == 'admin':
                        # Check if cpanel is activated. If not redirect to activation page
                        request.ldap.search(
                            search_base   = settings.LDAP_TREE_BASE ,
                            search_filter = "(&(objectClass=extensibleObject)(cn=%s))" % request.user.username,
                            attributes    = ['status']
                        )
                        admin_status = request.ldap.entries[0].status.value
                        #admin_status = 'active'
                        # Redirect only if page is not activate - avoid infinite redirection
                        # Disallow any other page until user has been activated
                        #last_path = request.path.rsplit('/', 1)[-1]
                        if (admin_status !='active' and request.path.rsplit('/',1)[-1]=='logout'):
                            # Do nothing. Allow user to logout from activate page.
                            # not elegant way.....but avoids infinite redirection   
                            print('log out')
                        elif (admin_status !='active' and request.path.rsplit('/',1)[-1]!='activate'):
                            return HttpResponseRedirect( reverse('activate') )
                        # Disallow activate page once user has been activated
                        elif (admin_status =='active' and request.path.rsplit('/',1)[-1]=='activate'):
                            return HttpResponseRedirect( reverse('logout') )
                        # get enabled services
                        request.ldap.search(
                            search_base   = settings.LDAP_TREE_SERVICES,
                            search_filter = settings.LDAP_FILTERS_INSTALLED_ENABLED_SERVICES,
                            attributes    = ['ou']
                        )
                        active_apps = [service.ou.value for service in request.ldap.entries]
                        setattr(request, "enabled_services", [service.ou.value for service in request.ldap.entries])
                        # get system reboot info
                        request.ldap.search(
                            search_base   = settings.LDAP_TREE_REBOOT,
                            search_filter = settings.LDAP_FILTERS_REBOOT,
                            attributes    = ['info']
                        )
                        setattr(request, "system_reboot", request.ldap.entries[0].info.value)
                        # get puppet status
                        setattr(request, "status", utils.get_puppet_status(request))

                        """ 
                        Get Release information from API and inject it into the request.
                        We've to set configuration links for any app
                        """
                        release_info = utils.get_release_info(request)
                        for app in release_info['configurations']:
                            id = app['settings']['id']
                            if id in active_apps and len(app['links']) > 0 and app['links'][0]['url'] == 'ldap':
                                try:
                                    request.ldap.search(
                                        search_base   = 'ou=domain,ou=%s,%s' % (id, settings.LDAP_TREE_SERVICES),
                                        search_filter = '(objectClass=organizationalUnit)',
                                        attributes    = ['status']
                                    )
                                    if(request.ldap.entries[0].status.value):
                                        app['links'][0]['url'] = request.ldap.entries[0].status.value
                                except LDAPNoSuchObjectResult:
                                        app['links'][0]['url'] = '/'+ id
                                except Exception as e:
                                    print("Middleware: There was a problem retrieving the domain list: %s" % e)
                        
                        setattr(request, "release", release_info)
                        
                elif ldap['error'] == 'LDAPSocketOpenError':
                    # If socket error, ldap is shut down for some reasons
                    # warn the user accordingly
                    del request.session['user']
                    messages.warning(request, _(
                        'Hay problemas de conectividad con la base de datos. '
                        'Intenta entrar dentro de unos minutos. Disculpa las '
                        'molestias.'
                    ))
                    return HttpResponseRedirect( login_url )
                else:
                    # If cannot connect to LDAP probably it has to do with LDAP's session
                    # expiration, so redirect to login.
                    """ destroy cookie """
                    # del request.session['user']
                    # del request.session['enc_pass']
                    messages.error(request, _('Tu sesión ha caducado, por favor introduce tus credenciales nuevamente'))
                    return HttpResponseRedirect( login_url )

            # if the session is closed redirect to login
            except AttributeError:
                return HttpResponseRedirect( login_url )
                
        return self.get_response(request)
