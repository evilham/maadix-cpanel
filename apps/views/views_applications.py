# python
import os, re
# django
#from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext_lazy as _, get_language
from django.contrib import messages
# contrib
from ldap3 import HASHED_SALTED_SHA, MODIFY_REPLACE
from ldap3.utils.hashed import hashed
from ldap3.core.exceptions import LDAPNoSuchObjectResult
# project
from .utils import get_release_info, connect_ldap, get_puppet_status, p, lock_cpanel, get_server_ip, get_server_host, domain_is_in_use, get_existing_domains, ldap_val
from .forms import EditAppForm 
from django.conf import settings

class AppSettings(FormView):
    """
    Edit applications setting for dependencies 
    """
    
    template_name   = 'pages/apps.html'
    form_class      = EditAppForm
    field_values    = {}
    success_url     = reverse_lazy('apps-settings')
    application_id  = None
    field_values    = {}

    def dispatch(self, request, *args, **kwargs):
        
        self.application_id   = self.kwargs.get('appid', None) 
        release = get_release_info(request)
        self.application_name = [ 
            service['settings']['name'] for service in release['configurations']
            if service['settings']['id'] == self.application_id 
        ][0]
        for service in release['configurations']:
          if service['settings']['id'] == self.application_id:
            print (service['fieldlist'])
            self.fields = [ 
              field for field in service['fieldlist'] 
              if field['editable']
            ]
        self.maintenance = False
        puppet_status = get_puppet_status(request)
        if puppet_status['puppetstatus'] == 'error' or puppet_status['puppetstatus'] == 'pending':
            self.maintenance = True
            
        return super(AppSettings, self).dispatch(request, *args, **kwargs)
        

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        
        context = super(AppSettings, self).get_context_data(**kwargs)
        context['appname']      = self.application_name 
        context['maintenance']  = self.maintenance
        context['show_modal']   = 'form' in kwargs
        context['update_group'] = 'form' in kwargs
        return context


    def get_form(self):
        """
        Return an instance of the form to be used in this view,
        Populate dynamic dependencies form fields with the values from ldap
        """

        try:
            for field in self.fields:
                fid = field['fid']
                dn = 'ou=%s,ou=%s,%s' % (
                    fid, 
                    self.application_id, 
                    settings.LDAP_TREE_SERVICES
                )
                self.request.ldap.search(
                    dn,
                    "(objectclass=*)",
                    attributes = ['status']
                )
                self.field_values[fid] = self.request.ldap.entries[0].status.value
        except Exception as e:
            print(e)
        return EditAppForm(
            fieldlist      = self.fields,
            maintenance    = self.maintenance, 
            application_id = self.application_id, 
            request        = self.request, 
            **self.get_form_kwargs()
        )

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = self.field_values
        return kwargs

    def post(self, request, *args, **kwargs):
        fields      = []
        data        = self.get(request)
        has_changed = False
        defaults    = self.get_form_kwargs()
        context     = self.get_context_data(**kwargs)
        form        = self.get_form()
        old_domain  = False
        if form.is_valid():
            context['update_group'] = True
            context['show_modal'] = True

        if 'writeldap' in request.POST: 
            for field in self.fields:
                fid       = field['fid']
                value     = form[fid].value() 
                if fid in form.changed_data:
                    try:
                        old_value = defaults['initial'][fid]
                    except:
                        old_value = False
                    # Update field, create ldap object if not exists
                    dn = 'ou=%s,ou=%s,%s' % (
                        fid,
                        self.application_id, 
                        settings.LDAP_TREE_SERVICES
                    )
                    try:
                      request.ldap.add(dn, ['organizationalUnit', 'metaInfo', 'top'], {
                        'status'            : value,
                      })
                    except Exception as e:
                        if e.result == 68: 
                            request.ldap.modify(dn, {
                              'status' : [(MODIFY_REPLACE, value)]
                            })
                    
                    # If filed is domain, record old value in ldap for puppet to be able to delete old domain
                    if fid == 'domain':
                        base_old_dn = 'ou=domain_old,ou=%s,%s' % (
                            self.application_id, 
                            settings.LDAP_TREE_SERVICES
                        )
                        try: 
                            request.ldap.search(
                                base_old_dn,
                                "(&(objectClass=organizationalUnit)(objectClass=metaInfo))",
                            )
                            if request.ldap.entries:
                                try: 
                                    request.ldap.modify(
                                        base_old_dn, 
                                        { 'status' : [ (MODIFY_REPLACE, str(old_value)) ] }
                                    )

                                except Exception as e:
                                    messages.error(request, _(
                                        "ha habido un problema actualizando la información"
                                    ))
                                    p("view_applications.py", "✕ There's was a problem updating old domain value", e)

                        except LDAPNoSuchObjectResult:
                            try: 
                                request.ldap.add(base_old_dn, [
                                    'organizationalUnit',
                                    'metaInfo',
                                    'top'
                                ], { 
                                    'status' : str(old_value) 
                                })

                            except Exception as e:
                                messages.error(request, _(
                                    "ha habido un problema actualizando la información"
                                ))
                                p( "view_applications.py", 
                                    "✕ There's was a problem creating old domain value", 
                                    e
                                )

            lock_cpanel(self.request) 
            
            return HttpResponseRedirect( reverse('logout') )

        return self.render_to_response(context)
