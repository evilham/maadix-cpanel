# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy
from django.contrib import messages
from django.core.validators import validate_email
# contrib
from ldap3 import HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
from ldap3 import MODIFY_REPLACE
# project
from . import utils
from .forms import EmailForm, EditEmailForm
from django.conf import settings


class MailAccounts(FormView):
    """
    Email accounts list view and form to add a new email account
    """

    template_name = 'pages/mail-accounts.html'
    form_class    = EmailForm
    domains       = {}

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        self.domain = self.request.GET.get('domain', None)
        context = super(MailAccounts, self).get_context_data(**kwargs)
        if self.domain:
            context['warnings'] = {}
            context['warnings']['dns']  = not 'MX' in utils.get_dns_records(self.domain)
            context['warnings']['mail'] = not utils.check_mail_active(self.request.ldap, self.domain)
        context['mails']        = self.mails
        context['display_form'] = 'form' in kwargs
        context['domain']       = self.domain
        return context
    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = {'at' : '@'}
        if self.domain:
            kwargs['initial'] = {'domain' : self.domain,'at' : '@' }
        return kwargs

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        self.domain = self.request.GET.get('domain', None)
        domains, emails = [], []
        # if admin show all domains and emails related
        if self.request.user.role == 'admin':
            domains = utils.get_existing_domains(self.request.ldap)
            self.domains = [ domain.vd.value for domain in domains ]
            emails  = utils.get_existing_emails(self.request.ldap)
        # if webmaster show only the domain administered by the user
        else:
            domain = self.request.user.username.split('@')[1]
            self.domains = [ domain ]
            emails  = utils.get_existing_emails(self.request.ldap, domain)

        self.mails = [{
            'name'   : entry.cn.value,
            'email'  : entry.mail.value,
            'domain' : entry.mail.value.split('@')[1],
            'dn'     : entry.entry_dn,
        } for entry in emails ]
        self.mails.sort(key=lambda x: x['domain'])
        if self.domain:
            self.mails = [ mail for mail in self.mails if mail['domain'] == self.domain ]
        return EmailForm(domains=self.domains, **self.get_form_kwargs())

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        email_name = form['email'].value()
        domain     = form['domain'].value()
        email      = '%s@%s' % (email_name, domain)
        password   = hashed(HASHED_SALTED_SHA, form['password'].value())
        name       = form['name'].value() if form['name'].value() else ''
        surname    = form['surname'].value() if form['surname'].value() else ''
        dn = 'mail=%s,vd=%s,%s' % (email, domain, settings.LDAP_TREE_HOSTING)
        classes= ['top', 'VirtualMailAccount', 'Vacation', 'VirtualForward', 'amavisAccount']

        data = {
                'accountActive'           : 'TRUE',
                'lastChange'              : utils.unix_timestamp(),
                'creationDate'            : utils.ldap_creation_date(),
                'amavisspamtaglevel'      : '3.0',
                'amavisspamtag2level'     : '5.5',
                'amavisspamkilllevel'     : '6.0',
                'amavisbypassviruschecks' : 'TRUE',
                'amavisbypassspamchecks'  : 'FALSE',
                'forwardactive'           : 'FALSE',
                'vacationactive'          : 'FALSE',
                'smtpauth'                : 'TRUE',
                'delete'                  : 'FALSE',
                'mailautoreply'           : email,
                'uid'                     : email_name + '.' + domain,
                'mailbox'                 : domain + '/' + email_name,
                'vdhome'                  : '/home/vmail/domains',
                'quota'                   : '0',
                'mail'                    : email,
                'userPassword'            : password,
                'sn'                      : surname,
                'givenname'               : name,
                'cn'                      : name + ' ' + surname,
            }
        try:
            self.request.ldap.add(dn,classes,data)
            """
            self.request.ldap.add(dn, ['top', 'VirtualMailAccount', 'Vacation', 'VirtualForward', 'amavisAccount'], {
                'accountActive'           : 'TRUE',
                'lastChange'              : utils.unix_timestamp(),
                'creationDate'            : utils.ldap_creation_date(),
                'amavisspamtaglevel'      : '3.0',
                'amavisspamtag2level'     : '5.5',
                'amavisspamkilllevel'     : '6.0',
                'amavisbypassviruschecks' : 'TRUE',
                'amavisbypassspamchecks'  : 'FALSE',
                'forwardactive'           : 'FALSE',
                'vacationactive'          : 'FALSE',
                'smtpauth'                : 'TRUE',
                'delete'                  : 'FALSE',
                'mailautoreply'           : email,
                'uid'                     : email_name + '.' + domain,
                'mailbox'                 : domain + '/' + email_name,
                'vdhome'                  : '/home/vmail/domains',
                'quota'                   : '0',
                'mail'                    : email,
                'userPassword'            : password,
                'sn'                      : surname,
                'givenname'               : name,
                'cn'                      : name + ' ' + surname,
            })
            """
            #utils.insert_mail_account(self.request.ldap,data)
            utils.notify_email(email)
            messages.success(self.request, _('Cuenta de correo añadida con éxito'))
        except Exception as e:
            if e.result == 68:
                error='';
                # abuse and postmaster are created by default,  with different classes.
                # but were not accessible by users.
                # Fix to let user create them- 
                if(email_name=='postmaster' or email_name=='abuse'):
                    try:
                        self.request.ldap.search(
                        dn,
                        '(objectClass=VirtualMailAccount)',
                        attributes = 'cn'
                        )
                        if(self.request.ldap.entries[0]):
                            error = _('La cuenta de correo que quieres crear ya existe. ¿Has introducido bien los datos?')
                    # Account has not been created yet
                    except Exception as e:
                        try:
                            utils.recursive_delete(self.request.ldap, dn)
                            #Add real account
                            self.request.ldap.add(dn,classes,data)
                            messages.success(self.request, _('Cuenta de correo añadida con éxito'))
                        except Exception as e:
                            error = _('Ha habido problemas creando la cuenta de correo, prueba de nuevo.')
                            utils.p("MailAccounts view", "There was a problem retreiving account", e)
                else:
                    error = _('La cuenta de correo que quieres crear ya existe. ¿Has introducido bien los datos?')
            else:
                error = _('Ha habido problemas creando la cuenta de correo, prueba de nuevo.')
                utils.p("MailAccounts view", "There was a problem creating email account", e)
            if(error!=''):
                messages.error(self.request, error)

        return super().form_valid(form)


class MailAccount(FormView):
    """
    Single email account view and update form
    """

    template_name   = 'pages/mail-account.html'
    form_class      = EditEmailForm
    ldap            = {}
    mail_account    = ''
    ldap_attributes = [
        'givenName',
        'sn',
        'DN',
        'forwardActive',
        'maildrop',
        'vacationActive',
        'vacationinfo'
    ]

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get(self, request):
        """Handle GET requests: instantiate a blank version of the form."""
        self.mail_account = request.GET['mail'] if not request.GET['mail'] == 'personal' else request.user.username
        print("REquwwwwwwwwwww " , self.mail_account)
        return super(MailAccount, self).get(request)
    def post(self, request):
        #Handle POST requests: process the form.
        self.mail_account = request.GET['mail'] if not request.GET['mail'] == 'personal' else request.user.username
        print("REquwwwwwwwwwww " , self.mail_account)
        return super(MailAccount, self).post(request)

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context = super(MailAccount, self).get_context_data(**kwargs)
        context['mail_account'] = self.mail_account
        context['fqdn'] = utils.get_server_host()
        # If error the form is not printed- See template
        context['error'] = self.error 
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        domain = self.mail_account.split('@')[1]
        try:
            self.mail_invalid = validate_email(self.mail_account)
            self.error = False
        except Exception as e:  
            self.error = True 

        if not self.error:
            dn='mail=%s,vd=%s,%s' % (self.mail_account,domain,settings.LDAP_TREE_HOSTING)
            try:
                self.request.ldap.search(
                    dn,
                    '(objectClass=VirtualMailAccount)',
                    attributes = self.ldap_attributes
                )
                account = self.request.ldap.entries[0]
                kwargs    = self.get_form_kwargs()
                kwargs['initial'] = { k : utils.ldap_val(account[k].value) for k in self.ldap_attributes }
                return EditEmailForm(mail_account=self.mail_account,**kwargs)
            except Exception as e:
                print(e)
        return EditEmailForm(mail_account=self.mail_account,**self.get_form_kwargs())

    def form_valid(self, form):
        """ Writing in ldap uses the dn returned by the ldapsearch.
        If the url parameter is changed, the dn will be the previous, 
        and it must exists
        """
        dn       = self.request.ldap.entries[0].entry_dn
        name     = form['givenName'].value()
        surname  = form['sn'].value()
        password = form['password'].value()
        domain   = self.mail_account.split('@')[1]
        new_values = {
            'givenName'      : name,
            'sn'             : surname,
            'cn'             : name + ' ' + surname,
            'lastChange'     : utils.unix_timestamp(),
            'forwardactive'  : utils.ldap_bool( form['forwardActive'].value() ),
            'maildrop'       : form['maildrop'].value(),
            'vacationactive' : utils.ldap_bool( form['vacationActive'].value() ),
            'vacationinfo'   : form['vacationinfo'].value(),
            'otherTransport' : "gnarwl:%s" % domain,
            'mailautoreply'  : self.mail_account+'.autoreply'
        }
        if password:
            new_values['userpassword'] = hashed(HASHED_SALTED_SHA, password)
        self.request.ldap.modify(dn, { k : [(MODIFY_REPLACE, v)] for k, v in new_values.items() })
        messages.success(self.request, _('Cuenta de correo modificada con éxito'))
        return super(MailAccount, self).form_valid(form)
