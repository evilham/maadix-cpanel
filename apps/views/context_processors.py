# python
import json, re
# django
from django.utils.text import slugify
# project
from django.conf import settings
from .utils import p, get_puppet_status, get_release_info
from . import debug

def site_info_processor(request):
    """Injects into global context information about the site"""

    html_document_title = settings.DOCUMENT_TITLE
    local = settings.LOCAL

    return locals()

def active_section_processor(request):
    """Injects into global context the slug of the path as an section identifier"""
    # Check if django is runnung in subpath
    prefix = settings.FORCE_SCRIPT_NAME
    active_section = slugify(request.path.replace(prefix, "", 1).lstrip('/'))
    return locals()

def ldap_status(request):
    """Injects into global context installed services"""

    enabled_services     = []
    enabled_services_info = []
    system_reboot        = False
    system_update        = False
    rocketchat_url       = ''
    notifications_number = 0
    apps_name_domain     = []
    maintenance          = False
    is_authenticated     = False

    if hasattr(request.user, 'role') and request.user.role == 'admin':
        is_authenticated = True
        enabled_services = request.enabled_services
        enabled_services_info = [ 
            conf for conf in request.release['configurations'] 
            if conf['settings']['id'] in enabled_services
        ]
        if(request.system_reboot == 'reboot'):
            system_reboot=True 
        if system_reboot:
            notifications_number += 1
        if(settings.NO_API):
            vm_status = debug.STATUS
            updates   = debug.UPDATES
        else:
            vm_status = request.status
            updates   = get_release_info(request, 'update')
        system_update = vm_status == 'pending' or vm_status == 'error' or updates=='True'
        if system_update:
            notifications_number+=1
        #maintenance = vm_status == 'pending' or vm_status == 'error'
        puppetstatus= vm_status.get('puppetstatus', None)
        maintenance = False if puppetstatus=='ready'  else True 
        #print('ENABLED SERVCE', enabled_services)
    return {
        'enabled_services'      : enabled_services,
        'enabled_services_info' : enabled_services_info,
        'system_reboot'         : system_reboot,
        'system_update'         : system_update,
        'notifications_number'  : notifications_number,
        'maintenance'           : maintenance,
        'is_authenticated'      : is_authenticated,
    }
