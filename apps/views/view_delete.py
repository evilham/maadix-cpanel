# django
from django import views
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.contrib import messages
# project
from . import utils
from django.conf import settings

class DeleteEntry(views.View):
    """ View to delete an entry from LDAP """

    def post(self, request):
        url = request.POST.get('url')
        # If Cpanel local domains is locked do not allow performing deletion. Tell user to waiut a minute an try again
        trash_status = utils.get_cpanel_local_status(self.request.ldap, 'domains')     
        if (trash_status != 'ready'):
            messages.warning(self.request, _('Hay otro proceso de borrado en curso. '
           'Vuelve a eliminar el elemento pasado un minuto, cuando el proceso anterior haya terminado'))
        else: 
            try:
                dn  = request.POST.get('dn')
                # Make sure item to delete does not start with special characters.
                # Don't want to delete ../something or / :-]
                """
                if not utils.validate_username(dn):
                    message.error(self.request, _('No se puede borrar este elemento. Cadena de carácteres inválida'))
                """
                # Move item to trash
                if (('ou=sshd,ou=People,' in dn) or(dn.startswith("vd=") and settings.LDAP_TREE_HOSTING in dn)):
                    utils.move_to_trash(self.request.ldap, dn) 
                # Lock cpanel domains if a domains is deleted
                if (dn.startswith("vd=") and settings.LDAP_TREE_HOSTING in dn):
                   utils.lock_cpanel_local(self.request.ldap, 'domains'); 
                # Delete item recursively
                utils.recursive_delete(self.request.ldap, dn)
                messages.success(self.request, _('Elemento eliminado con éxito'))
                return HttpResponseRedirect(url)
            except Exception as e:
                if settings.DEBUG:
                    print(e)
                messages.error(self.request, _('Hubo algún problema eliminando la cuenta de correo. '
                                           'Contacta con los administrador-s si el problema persiste'))
        return HttpResponseRedirect(url)
