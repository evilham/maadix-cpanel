import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


class NumberValidator(object):
    def validate(self, password, user=None):
        if not re.findall('\d', password):
            raise ValidationError(
                _("La contraseña debe contener al menos una cifra 0-9."),
                code='password_no_number',
            )

    def get_help_text(self):
        return _(
            "La contraseña debe contener al menos una cifra 0-9."
        )


class UppercaseValidator(object):
    def validate(self, password, user=None):
        if not re.findall('[A-Z]', password):
            raise ValidationError(
                _("La contraseña debe contener al menos una mayúscula, A-Z."),
                code='password_no_upper',
            )

    def get_help_text(self):
        return _(
            "La contraseña debe contener al menos una mayúscula, A-Z."
        )


class LowercaseValidator(object):
    def validate(self, password, user=None):
        if not re.findall('[a-z]', password):
            raise ValidationError(
                _("La contraseña debe contener al menos una minúscula, a-z."),
                code='password_no_lower',
            )

    def get_help_text(self):
        return _(
            "La contraseña debe contener al menos una minúscula, a-z."
        )


class SymbolValidator(object):
    def validate(self, password, user=None):
        if not re.findall('[()[\]{}|\\`~!@#$%^&*_\-+=;:\'",<>./?]', password):
            raise ValidationError(
                _("La contraseña debe contener al menos un un carácter especial: " +
                  "()[]{}|\`~!@#$%^&*_-+=;:'\",<>./?"),
                code='password_no_symbol',
            )

    def get_help_text(self):
        return _(
            "La contraseña debe contener al menos un un carácter especial: " +
            "()[]{}|\`~!@#$%^&*_-+=;:'\",<>./?"
        )
