# python
import json
import os

# django
from django.utils.translation import ugettext_lazy as _, get_language
from django import views
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic.edit import FormView
# contrib
from ldap3 import MODIFY_REPLACE,SUBTREE
from collections import defaultdict
# project
from . import utils 
from django.conf import settings
from . import debug
from .forms import TrashForm 

class Trash(FormView):
    template_name = 'pages/trash.html'
    form_class    = TrashForm
    ldap          = {}

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        context = super(Trash, self).get_context_data(**kwargs)
        context['show_modal'] = 'form' in kwargs
        context['trashContent'] = self.trashContent
        if (self.trash_status == 'error'):
            context['modal_text_body'] =_("Si continúas se reanudará la última operación de borrado que generó un error y se eliminarán de forma permanente las carpetas previamente seleccionadas. Esta acción no se puede deshacer.")
        else:
            context['modal_text_body'] =_("Si continúas se borrarán de forma permanente los elementos seleccionados. Esta acción no se puede deshacer.")
        context['modal_text_confirm'] =_("¿Confirmas que quieres continuar?")
        context['trash_status'] = self.trash_status
        return context

    def get(self, request):
        switcher={'users':{
                    'description': _("Carpetas correspondientes a cuentas de usuarixs que has eliminado"),
                    'title': _('Usuarixs'),
                    'icon': 'fa fa-users'},
                  'domains':{
                    'description': _("Carpetas correspondientes a los dominios que has eliminado"),
                    'title': _('Dominios'),
                    'icon': 'fa fa-globe'},
                  'backups':{
                    'description': _("Estos archivos se han generado en el proceso de actualización de ciertas aplicaciones a modo de copia de seguridad. Una vez hayas comprobado que la actualización se realizó correctamente puedes eliminarlos. Asegúrate de que no los necesitas antes de proceder."),
                    'title': _('Copias de archivos de aplicaciones'),
                    'icon': 'fa fa-folder-open'}
                      
                    }

        trashContent={}
        trashContent['clean'] = {}
        trashContent['trash'] = {}
        #trashContent=defaultdict(lambda: defaultdict(dict))
        args = {} 
        subtrees_trash = ['domains', 'users','backups']
        trash_directory= '/home/.trash/'
        subtrees_clean =['docker', 'apt']
        # The list will be create fom directory
        for item in subtrees_trash:
            trashContent['trash'][item]={}
        for item in subtrees_clean:
            trashContent['clean'][item]={}
        # Get trash module status iis serror  show a button to rcover previous failed operation

        self.trash_status = utils.get_cpanel_local_status(self.request.ldap, 'trash')     
        for item in subtrees_clean:
            if item == 'docker':
                desc = _("Se eliminarán las imágenes de docker que no están referenciadas ni usadas por ningún contenedor. Si has instalado alguna imagen de docker de forma manual, sin utilizar el panel de control, asegúrate de no tender ninguna que no esté actualmente en uso y que quieras conservar.")
                label = _("Eliminar imágenes de Docker no usadas")
                name = item
                icon = "fa fa-archive"
            elif item == "apt":
                name =  _("Paquetes obsoletos")
                desc = _("Se ejecutará el comando apt-get clean para limpiar la cache de paquetes instalados. Se eliminarán los archivos que ya no son necesarios para el sistema.")
                label = _("Eliminar paquetes obsoletos")
                icon = "fa fa-angle-double-down"
            
            trashContent['clean'][item]['name']=item
            trashContent['clean'][item]['desc']=desc
            trashContent['clean'][item]['label']=label
            trashContent['clean'][item]['icon']=icon
        for item in subtrees_trash:
            trashContent['trash'][item]['description']=switcher[item]['description']
            trashContent['trash'][item]['title']=switcher[item]['title']
            trashContent['trash'][item]['icon']=switcher[item]['icon']
            trashContent['trash'][item]['files']={}
            try:
                self.request.ldap.search('ou=%s,%s' % (item, settings.LDAP_TREE_TRASH),
                    '(|(&(objectClass=metaInfo)(status=intrash))(&(objectClass=metaInfo)(status=totrash)))',
                    attributes=['type', 'otherPath','cn','description','status'])
                trashContent['trash'][item]['files']=self.request.ldap.entries 
            except Exception as e:
                messages.error(self.request, _('Se ha producido un error leyendo la Papelera'))
        dir_path = {"backups" : "/home/.trash/backups/"}
        # backups have different behaviour as they are not in ldap yet
        for dirKey, dirValue in dir_path.items():
            if(os.path.isdir(dirValue)):
                trashContent['trash'][dirKey]['files']={}
                listFiles = os.listdir(dirValue)
                # Gete elements in /home/backups
                filesToList = []
                filesToList = utils.list_files_in_trash(self.request.ldap,dirValue,dirKey,listFiles)
                trashContent['trash'][dirKey]['files']=filesToList
        # Add clean-apt and clean docker items 
        self.trashContent= trashContent
        
        return super(Trash, self).get(request)        

    def get_form(self):
        return self.form_class(request=self.request,trashContent=self.trashContent, **self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        data = self.get(request)
        form = self.get_form()
        context = self.get_context_data(**kwargs)
        cleanou =''
        # Get all checked iputs
        if form.is_valid() and request.method=='POST':
            context['show_modal'] = True

        if request.method=='POST' and 'writeldap' in request.POST:
            for field in form:
                # Difference files in  trash and docker images or apt purge
                if field.value(): 
                    if (field.name== "docker" or field.name=='apt'):
                        cleanou='clean' + field.name		
                        ldaptree = "ou=%s,%s" % ( cleanou,settings.LDAP_TREE_CPANEL)
                        errors=False
                        """Handle POST requests."""

                        try:
                            request.ldap.search(
                                ldaptree,
                                settings.LDAP_FILTERS_CLEAN,
                                attributes=['cn']
                            )
                            # if clean tree already exist in ldap update it
                            if self.request.ldap.entries:
                                try:
                                    request.ldap.modify(
                                        settings.LDAP_TREE_CLEAN, {
                                            'info'   : [(MODIFY_REPLACE, 'ready')],
                                            'status' : [(MODIFY_REPLACE, 'locked')],
                                        }
                                    )
                                    messages.success(request, _('Operación guardada con éxito' ))
                                except Exception as e:
                                    messages.error(request, _('Se ha producido un error' ))
                                    utils.p("Clean view", "Error updating clean entry in ldap", e)
                        except Exception as e:
                            utils.p("Clean view", "No clean entry found in ldap", e)
                            errors=True
                        if errors:
                            try:
                                # if clean tree does not exixts in ldap create it with needed value 
                                request.ldap.add(ldaptree, [
                                    'organizationalUnit',
                                    'metaInfo',
                                    'extensibleObject'
                                    ],
                                    {'info': 'ready', 'status': 'locked'})
                                messages.success(request, _('Operación guardada con éxito' ))
                            except Exception as e:
                                messages.error(request, _('Se ha producido un error' ))
                                utils.p("Clean view", "Error creating  clean entry in ldap", e)
                    else:
                        # Need to check if is backup file 
                        if(field.label):
                            label_value= field.label
                            label_value= label_value.split("-")
                            field_type = label_value[0]
                            field_inode = label_value[1]
                            base    =  settings.LDAP_TREE_TRASH
                            filters = '(&(objectClass=applicationProcess)(cn=%s)(l=%s))'%(field.name,field_inode)
                            attributes    = [ 'cn','l']
                            is_backup=True

                        else:
                            base    = settings.LDAP_TREE_TRASH
                            filters ='(&(objectClass=applicationProcess)(cn=%s))'%field.name
                            attributes    = [ 'cn']
                            is_backup = False
                        try: 
                            self.request.ldap.search(
                                base,
                                filters,
                                attributes=attributes
                            )
                            dn = self.request.ldap.entries[0].entry_dn
                            if(dn):
                                # Entry was prersent
                                try:
                                    self.request.ldap.modify(dn, { 'status' : [(MODIFY_REPLACE, 'purge')] })
                                except Exception as e:
                                    utils.p("vie_trash.py · move_to_trash", "x error nodifying ldap entry" , e)
                        except Exception as e:
                            utils.p("vie_trash.py · move_to_trash", "No entry found in ldap" , e)
                            try:
                                utils.move_file_to_trash(self.request.ldap, field_type, field.name, field_inode)
                            except Exception as e:
                                utils.p("vie_trash.py · move_to_trash", "x error writing lo ldap" , e)
                                messages.error(self.request, _('Se ha producido un error'))
                    utils.lock_cpanel_local(self.request.ldap, 'trash')
                    # Put here the code for locking cpael-puppet clean modules
            context['show_modal'] = False
            messages.success(self.request, _('Cambios aplicados con éxito'))
            #utils.lock_cpanel_local(self.request.ldap, 'trash')
            return HttpResponseRedirect(self.request.get_full_path())

        return self.render_to_response(context)
