# python
import urllib3, os, json, pwd, certifi

# django
from django.utils.translation import ugettext_lazy as _, get_language
from django import views
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.generic.edit import FormView

# contrib
from ldap3 import MODIFY_REPLACE
import gnupg

# project
from .utils import get_release_info, connect_ldap, get_puppet_status, p, lock_cpanel, get_server_ip,split_dependencies, update_ldap_entry
from django.conf import settings
from . import debug
from .forms import InstallAppForm, UpdateAppForm

class Services(FormView):
    template_name = 'pages/services.html'
    form_class    = UpdateAppForm
    ldap          = {}
    ldap_attributes = ['status']
    dep_status    = {}

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):

        context = super(Services, self).get_context_data(**kwargs)
        context['not_disabled'] = ['mail', 'nodejs']
        context['show_modal']   = 'form' in kwargs
        context['enabled']      = self.enabled_services
        context['disabled']     = self.disabled_services
        context['all_services'] = self.all_services
        return context

    def get(self, request):
        self.puppet_status = get_puppet_status(request)
        self.all_services  = []

        try:
            self.enabled_services = self.request.enabled_services
    	    # Exclude installed services from available services
    	    # self.services_available = [ service for service in all_services if service['id'] not in installed_services ]
            request.ldap.search(
        		settings.LDAP_TREE_BASE,
        		settings.LDAP_FILTERS_INSTALLED_DISABLED_SERVICES,
        		attributes=['ou']
    	    )
            self.disabled_services = [ service.ou.value for service in request.ldap.entries ]
            confs = request.release.get('configurations')

            if confs: 
                self.all_services = [ s for s in confs if ( 
                    s['settings']['id'] in self.enabled_services or
                    s['settings']['id'] in self.disabled_services 
                )]

        except Exception as e:
            # Maybe mark a difference between no available aplicaction due to 'All of them have been installed' and a 
            # problem retreiving available application from ldap or whatever error
            # Now both cases are the same and sets context['no_updates']  = True
            p("Services view", "✕ There was a problem retrieving enabled services", e)
        
        return super(Services, self).get(request)

    def get_form(self):
        return self.form_class(
            enabled_services  = self.enabled_services,
            disabled_services = self.disabled_services,
            all_services      = self.all_services, 
            **self.get_form_kwargs()
        )

    def post(self, request, *args, **kwargs):
        
        data    = self.get(request)
        form    = self.get_form()
        context = self.get_context_data(**kwargs)

        disable_services   = []
        enable_services    = []
        enable_group_deps  = []
        disable_group_deps = []
        deps_needed        = []
        
        if form.is_valid():

            for service in self.all_services:
                app_name   = service['settings']['id']
                
                # Applications to be disabled
                field_name = "disable-%s" % app_name 
                value      = form[field_name].value() if field_name in form.fields else None
                if app_name in self.enabled_services and value:
                    disable_services.append( app_name ) 
                    # If app_name has a dependency check if it can be
                    # disabled or if is in use by another app
                    if service['dependencies']:   
                        field_name = "dependency-%s" % app_name
                        value      = form[field_name].value() if field_name in form.fields else None
                        if value and not value in context['not_disabled']:
                            disable_group_deps.append( value )

                # Applications to be enabled                
                field_name = "enable-%s" % app_name
                value      = form[field_name].value() if field_name in form.fields else None
                
                if app_name in self.disabled_services and value:
                    enable_services.append( app_name )
                    if service['dependencies']:
                        field_name = "dependency-%s" % app_name
                        value      = form[field_name].value() if field_name in form.fields else None               
                        if value and (value not in enable_group_deps or value not in enable_services):
                            enable_group_deps.append(value)

            # Check if candidates dependencies can be disabled or if are used from other apps
            # Need to check this after post, after groups are checked and actions are defined
            for service in self.all_services:
                for dependency in service['dependencies']:
                    if dependency['id'] in self.enabled_services:
                        deps_needed.append( dependency['id'] )

            # Used in modal confirmation to list apps to be enabled/disabled
            # Remove items from disable_group_deps if are already in disable_services
            # TODO: If a service with dependency A is to be disabled and at the same tine
            # another service with same dependency is to be 
            # enabled, do not disable dependency A.
            # For ex. if a dependency is marked to collect all emabled and disabled dependecies.
            # For any match, remove it from disable an leave it in enable 

            clean_disable_dep  = [ 
                item for item in disable_group_deps 
                if  item not in disable_services 
                and item not in enable_group_deps
            ]
            clean_enable_dep  = [ 
                item for item in enable_group_deps 
                if  item not in enable_services 
                and item not in self.enabled_services
            ]
            context['disable_groups']     = disable_services
            context['disable_dep_groups'] = clean_disable_dep
            context['enable_groups']      = enable_services
            context['enable_dep_groups']  = clean_enable_dep

            # Check if trying to disable a required app
            for service in disable_services:
                if service in enable_group_deps or (service in deps_needed and service not in disable_group_deps):
                    messages.error(
                        self.request, _( 
                           'La aplicación %s no se puede desactivar por que es '
                           'necesaria para otras aplicaciones seleccionadas o activadas' 
                           % service
                        )
                    )
                    return self.render_to_response(context)
                    
            context['show_modal'] = True
            
            # Check if app in dep_groups is in use
            if 'writeldap' in request.POST:
                
                services_enable  = enable_services + clean_enable_dep
                services_disable = disable_services

                try:
                    # update services to be disabled
                    if services_disable:
                        for service in services_disable:
                            dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                            request.ldap.modify(dn, {
                                'type'   : [(MODIFY_REPLACE, 'installed')],
                                'status' : [(MODIFY_REPLACE, 'disabled')],
                            })

                    # update services to be enabled
                    if services_enable:
                        for service in services_enable:
                            dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                            request.ldap.modify(dn, {
                                'type'   : [(MODIFY_REPLACE, 'available')],
                                'status' : [(MODIFY_REPLACE, 'enabled')],
                            })
                            
                    # lock cpanel
                    lock_cpanel(request)

                    #messages.success(self.request, _('Aplicaciones modificadas con éxito'))
                    return HttpResponseRedirect( reverse('logout') )

                except Exception as e:
                    messages.error(self.request, _(
                        'Hubo algún problema modificando el estado de tus aplicaciones. '
                        'Contacta con los administrador-s si el problema persiste'
                    ))
                    print(e)
                    context['show_modal'] = False
                    return self.render_to_response(context)

        else:
            context['show_modal'] = False 
        #return self.form_invalid(form, **kwargs)
        return self.render_to_response(context)

class AvailableAppsview(FormView):
    template_name = 'pages/appsinstall.html'
    form_class    = InstallAppForm
    ldap          = {}
    ldap_attributes = ['status']
    dep_status    = {}
    inputdeps     = []
    #maintenance   = False

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):

        context = super(AvailableAppsview, self).get_context_data(**kwargs)
        context['status'] = self.puppet_status
        context['release'] = self.release_info
        context['available'] = self.services_available
        context['all_installed'] = self.allinstalled
        context['no_updates'] = self.no_updates
        context['show_modal'] = 'form' in kwargs
        context['groups'] = 'form' in kwargs	
        context['dep_groups'] = 'form' in kwargs
        return context

    def get(self, request):
        #TODO: merge both requests
        self.release_info  = request.release
        self.puppet_status = get_puppet_status(request)
        all_services = []
        self.allinstalled = False
        self.no_updates = False

        try:
            all_services = self.release_info['configurations']
            request.ldap.search(
                settings.LDAP_TREE_BASE,
                settings.LDAP_FILTERS_INSTALLED_SERVICES_ALL,
                attributes=['ou']
                )
            installed_services = [ service.ou.value for service in request.ldap.entries ]
            # Exclude installed services from available services
            self.services_available = [ service for service in all_services if service['settings']['id'] not in installed_services ]
            if not self.services_available:
                self.allinstalled = True
        except Exception as e:
            # Maybe mark a difference between no available aplicaction due to 'All of them have been installed' and a 
            # problem retreiving available application from ldap or whatever error
            # Now both cases are the same and sets context['no_updates']  = True
            self.no_updates = True
            p("ServicesAvailable view", "✕ There was a problem retrieving enabled services", e)
            if self.puppet_status['puppetstatus'] == 'error' or self.puppet_status['puppetstatus'] == 'pending':
                self.maintenance= True
                
        return super(AvailableAppsview, self).get(request)

    def get_form(self):
        return InstallAppForm(
            request=self.request,
            services_available=self.services_available, 
            **self.get_form_kwargs()
        )    

    def post(self, request, *args, **kwargs):
        data = self.get(request)
        form = self.get_form()
        # an empty list to store all gorups to be installed
        services = []
        group_deps = []
        # an empty dict to store user input dependencies
        dependencies = {}
        dependencies["app"] = []
        context = self.get_context_data(**kwargs)

        if form.is_valid():
            for service in self.services_available:
                appname = service['settings']['id']
                # First get checked application
                if form[appname].value():
                    # Add app name to apps to be installed in ldap
                    services.append(appname)    
                    for app in service['dependencies']:
                        fieldname = "dependency-%s" % appname
                        if form[fieldname].value():
                            group_deps.append( app['id'] )
                    for field in service['fieldlist']:
                        fieldname  = 'input-%s-%s' % ( appname, field['fid'] )
                        if form[fieldname].value():
                            args = {
                                'name'   : field['fid'],
                                'service': appname,
                                'editable': field['editable'],
                                'value'  : form[fieldname].value().strip(),
                            }
                            dependencies["app"].append(args)
            
            context['show_modal'] = True
            context['groups']     = services
            context['dep_groups'] = group_deps
            
            if 'writeldap' in request.POST:
                services += group_deps
                print(services)
                for service in services:
                    DN = "ou=%s,%s" % ( service, settings.LDAP_TREE_SERVICES )
                    print("Installing service '%s' with DN '%s'" % (service, DN))
                    if settings.DEBUG_INSTALL:
                        print("Installing service '%s' with DN '%s'" % (service, DN))
                    else:
                        update_ldap_entry(
                            request.ldap, DN,
                            { 
                                'status' : [ (MODIFY_REPLACE, 'install') ] 
                            },
                            "✕ service not found ",
                            {
                                'ou'     : service,
                                'status' : 'install',
                                'type'   : 'available',
                            },
                            "✕ could not add service ",
                        )
                # Add dependencies
                # This are user input dependencies, that are inserted as subtree of the app they belong
                for dependency in dependencies['app']:
                    print(dependency)
                    #crypt not ediatable passwords fields
                    if dependency["name"]=='password' and not dependency["editable"]:
                        #crypt only if installed in production
                        try:
                          gpg = gnupg.GPG(gnupghome="/usr/share/mxcp/.gnupg")
                          public_keys = gpg.list_keys()
                          fingerprint = public_keys[0]['fingerprint']
                          encrypted_password = gpg.encrypt(dependency["value"], fingerprint,always_trust=True)
                          dependency["value"] = str(encrypted_password)
                        except:
                          pass
                    create_dep = False
                    DN = 'ou=%s,ou=%s,%s' % ( 
                        dependency["name"], 
                        dependency["service"], 
                        settings.LDAP_TREE_SERVICES
                    )
                    if settings.DEBUG_INSTALL:
                        print("Installing dep '%s' with DN '%s'" % ( dependency, DN ))
                        print( dependency['value'] )
                    else:
                        update_ldap_entry(
                            request.ldap, DN,
                            {
                                'status' : [ ( MODIFY_REPLACE, dependency["value"] ) ]
                            },
                            "✕ entry for service deps does not exists",
                            {
                                'ou'     : dependency["name"],
                                'status' : dependency["value"],
                            },
                            "✕ Could not create dependency tree",
                        )
                lock_cpanel(request)
                # messages.success(self.request, _('Aplicaciones instaladas con éxito'))
                return HttpResponseRedirect( reverse('logout'))

        else:
            context['show_modal'] = False 
        #return self.form_invalid(form, **kwargs)
        return self.render_to_response(context)
