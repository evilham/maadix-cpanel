# python
import os
# django
from django import views
from django.views.generic.edit import FormView
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext_lazy as _
# contrib
from ldap3 import HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
from ldap3 import MODIFY_REPLACE
# project
from . import utils
from .forms import AddDomainForm, EditDomainForm
from django.conf import settings


class DomainsListView(views.View):
    """
    Domains list view.
    """
    success_url   = reverse_lazy('domains')
    def get(self, request):
        domains = utils.get_existing_domains(request.ldap)
        domains_module = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        admin = utils.get_admin(self.request.ldap)
        return render(request, 'pages/domains.html', locals())

    def post(self, request):
        if request.method=='POST' and 'writeldap' in request.POST:
            try: 
                utils.lock_cpanel_local(self.request.ldap, 'domains');
                messages.success(self.request, _('Reanudando operación previa. El proceso tardará unos minutos'))
            except Exception as e:
                print(e)
        return HttpResponseRedirect(self.success_url)

class AddDomainView(FormView):
    """
    Form to add a new domain.
    """

    template_name = 'pages/domains-add.html'
    form_class    = AddDomainForm
    success_url   = reverse_lazy('domains')

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        domains_module = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        context = super(AddDomainView, self).get_context_data(**kwargs)
        context['domains_module'] = domains_module
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        domains_module = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        # Get users to display them on the webmaster select list
        self.request.ldap.search(
            settings.LDAP_TREE_USERS,
            settings.LDAP_FILTERS_USERS_WEBMASTER_FORM,
            attributes=['uid']
        )
        users = [ user.uid.value for user in self.request.ldap.entries ]
        #print('USERS IN VIEW ' , users)
        users.insert(0,'nobody')
        """
        self.request.ldap.search(
            settings.LDAP_TREE_USERS,
            settings.LDAP_FILTERS_SUDOERS,
            attributes=['uid']
        )
        superuser = self.request.ldap.entries[0].uid.value
        users.insert(0,superuser)
        """
        # List of existing domains, for form validation
        domains = utils.get_existing_domains(self.request.ldap)
        domain_list = [ domain.vd.value for domain in domains ]
        domains_in_use = utils.domain_is_in_use(self.request.ldap)
        domains_used_list = [ domain.status.value for domain in domains_in_use]
        return AddDomainForm(request=self.request,users=users, domain_list=domain_list, domains_used_list=domains_used_list,**self.get_form_kwargs())

    def form_valid(self, form, **kwargs):
        """ Hook to be triggered if form is valid. """

        domain = form['name'].value().strip()
        mail   = str(form['mail_server'].value()).upper()
        admin  = form['webmaster'].value() if form['webmaster'].value() else 'nobody'
        dkim   = form['dkim'].value()

        try:
            # Add domain
            dn = 'vd=%s,o=hosting,%s' % (domain, settings.LDAP_TREE_BASE)
            self.request.ldap.add(dn, ['VirtualDomain', 'top', 'Yap'], {
                'vd'            : domain,
                'adminid'       : admin,
                'accountActive' : mail,
                'lastChange'    : utils.unix_timestamp(),
                'delete'        : 'FALSE',
                'otherPath'     : 'default',
            })
            # Create postmaster
            postmaster_dn  = 'cn=postmaster,' + dn
            postmaster_pwd = utils.gen_pwd()
            self.request.ldap.add(postmaster_dn, ['VirtualMailAlias', 'top'], {
                'cn'            : 'Postmaster',
                'sn'            : 'Postmaster',
                'mail'          : 'postmaster@%s' % domain,
                'maildrop'      : 'postmaster',
                'accountActive' : mail,
                'creationDate'  : utils.ldap_creation_date(),
                'lastChange'    : utils.unix_timestamp(),
                'userpassword'  : hashed(HASHED_SALTED_SHA, postmaster_pwd)
            })
            # Add abuse email
            mail     = 'abuse@' + domain
            abuse_dn = 'mail=%s,%s' % (mail, dn)
            self.request.ldap.add(abuse_dn, ['VirtualMailAlias', 'top'], {
                'cn'            : 'abuse',
                'sn'            : 'abuse',
                'mail'          : mail,
                'maildrop'      : 'postmaster',
                'accountActive' : 'TRUE',
                'creationDate'  : utils.ldap_creation_date(),
                'lastChange'    : utils.unix_timestamp()
            })
            # DKIM
            if dkim:
                utils.add_dkim(self.request.ldap, domain)

            # If Rainloop installed check if con file exists, else creates it
            if 'rainloop' in self.request.enabled_services:
                utils.check_rainloop_conf(domain)
            dns_records = utils.get_dns_records(domain)
            record_message = utils.check_dns_A_record(dns_records)
            if record_message['error']:
                messages.warning(self.request, _("No se puede activar el servidor web porque los DNS para el dominio %s no están apuntando a la IP de este servidor. Crea la configuración DNS necesaria desde la interfaz de tu proveedor de dominio y luego activa el servidor web desde la página de edición del dominio de este panel de control." % domain))
            else:
                messages.success(self.request, _('Dominio %s añadido con éxito' % domain))
                """ Lock domain in the panel to trigger puppet local for domain creation """
            utils.lock_cpanel_local(self.request.ldap, 'domains');

        except Exception as e:
            utils.p("view_domains.py", "✕ There's was a problem creating the domain %s" % domain, e)
            messages.error(self.request, _('Ha habido un problema creando el dominio %s.' % domain))

        return super(AddDomainView, self).form_valid(form)

    def post(self, request):
        form = self.get_form()
        if request.method=='POST' and 'writeldap' in request.POST:
            try:
                utils.lock_cpanel_local(self.request.ldap, 'domains');
                messages.success(self.request, _('Reanudando operación previa. El proceso tardará unos minutos'))
            except Exception as e:
                print(e)
            return HttpResponseRedirect(self.success_url)
        else:
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)


class EditDomainView(FormView):
    """
    Form to edit a new domain.
    """

    template_name = 'pages/domain.html'
    form_class    = EditDomainForm
    success_url   = reverse_lazy('domains')

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        domains_module = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        self.domain_name = self.request.GET.get('domain')
        context = super(EditDomainView, self).get_context_data(**kwargs)
        context['domain'] = self.domain_name
        context['domains_module'] = domains_module
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        self.domain_name = self.request.GET.get('domain')
        domain      = utils.get_domain(self.request.ldap, self.domain_name)
        has_dkim    = utils.has_dkim(self.request.ldap, self.domain_name)
        kwargs = self.get_form_kwargs() 
        print('Domain is: ', self.domain_name)
        webserver = os.path.isfile('/etc/apache2/ldap-enabled/%s-ssl.conf' % self.domain_name)
        print('web server is: ' , webserver)
        kwargs['initial'] = {
            'webServer'   : webserver,
            'webmaster'   : domain.adminid.value,
            'mail_server' : utils.ldap_val(domain.accountActive.value),
            'dkim'        : has_dkim,
            'old_dkim'    : has_dkim,
        }
        #users = utils.get_users(self.request.ldap)
        # Get users to display them on the webmaster select list
        self.request.ldap.search(
            settings.LDAP_TREE_USERS,
            settings.LDAP_FILTERS_USERS_WEBMASTER_FORM,
            attributes=['uid']
        )
        users = [ user.uid.value for user in self.request.ldap.entries ]
        users.insert(0,'nobody')
        if domain.adminid.value not in users:
            users.insert(1,domain.adminid.value)

        return EditDomainForm(users=users, domain=self.domain_name,request=self.request, **kwargs)

    def form_valid(self, form):
        dn = 'vd=%s,%s' % (self.domain_name, settings.LDAP_TREE_HOSTING)
        webmaster = form['webmaster'].value()
        mail      = form['mail_server'].value()
        dkim     = bool(form['dkim'].value())
        old_dkim = utils.ldap_val(form['old_dkim'].value())
        try:
            self.request.ldap.modify(dn, {
                'adminid'       : [( MODIFY_REPLACE, webmaster )],
                'accountActive' : [( MODIFY_REPLACE, utils.ldap_bool(mail) )],
            })
        except Exception as e:
            utils.p("✕ view_domains.py", "There was a problem updating the domain", e)
            messages.error(self.request, _('Ha habido un error modificando el dominio. '
                                           'Si el problema persiste contacta '
                                         'con los administrador-s'))
        if dkim != old_dkim:
            if dkim:
                utils.add_dkim(self.request.ldap, self.domain_name)
            else:
                utils.remove_dkim(self.request.ldap, self.domain_name)
        if 'webmaster' in form.changed_data or 'webServer' in form.changed_data:
            utils.lock_cpanel_local(self.request.ldap, 'domains');
        messages.success(self.request, _('Edición realizada con éxito. Espera unos segundos hasta que el sistema aplique la nueva configuración')) 
        return super(EditDomainView, self).form_valid(form)

    def post(self, request):
        form = self.get_form()
        if request.method=='POST' and 'writeldap' in request.POST:
            try:
                utils.lock_cpanel_local(self.request.ldap, 'domains');
                messages.success(self.request, _('Reanudando operación previa. El proceso tardará unos minutos'))
            except Exception as e:
                print(e)
            return HttpResponseRedirect(self.success_url)
        else:
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)

class MailmanDomains(views.View):

    def get(self, request, *args, **kwargs):
        active_mail_domains = utils.get_active_maildomains(request.ldap)
        mailman_domains     = utils.get_mailman_domains()
        active              = _('Activado')
        inactive            = _('Desactivado')
        processing          = _('Procesando')
        has_dkim = False
        # Get dkim status in cpanel..if locked the puppet apply is going to be run
        # and dkim for mailman domains will be automatically creates
        #real_domains = utils.connect_to_postgre() 
        #dkim_cpanel_status = utils.get_dkim_status_cpanel(request.ldap)

        for domain in mailman_domains:
            domain['dkim_status'] = False
            try:
                request.ldap.search(
                    "ou=%s,%s" % (domain['mail_host'],settings.LDAP_TREE_DKIM ),
		    "(objectClass=organizationalUnit)"
		)
		# Check if dkim certificate exists in server 
                dkim_path = "/etc/opendkim/keys/%s/default.txt" % domain['mail_host']
                if os.path.isfile(dkim_path):
                    domain['DKIM'] = "<span class='fa fa-power-off enabled'></span> %s" % active  
		    # ou=aaa.com,ou=opendkim,ou=cpanel,dc=example,dc=tld

                # If dkim status is not ready, dkim is going to be created
                else:        
                    domain['DKIM'] = "<span class='fa fa-spin fa-cog'></span> %s" % processing 
                    # Run puppet local  to create dkim for mailman domains 
                    #domain['dkim_status'] =True 

            except Exception as e:
                print("There was a problem retrieving DKIM entry in LDAP: ")
                print( str(e) )
                #domain['DKIM'] = _('La clave DKIM no está activada para este dominio')
                domain['DKIM'] = "<span class='fa fa-power-off disabled'></span> "
                domain['dkim_status'] =True

            #domain['dkim_status'] = False
            # Check vhost web status for given domain
            if domain['mail_host'] in active_mail_domains:
                domain['status'] = "<span class='fa fa-power-off enabled'></span> %s" % active
            else:
                domain['status'] = "<span class='fa fa-power-off disabled'></span> %s" % inactive
            # Check if dkim certificate exists in server 
        return render(request, 'pages/mailman-domains.html', locals())

class CreateDkim(views.View):
    """ View to delete an entry from LDAP """

    def post(self, request):
        try:
            dn  = request.POST.get('dn')
            url = request.POST.get('url')
            # Create  item 
            utils.add_dkim(self.request.ldap, dn)
            # lock dkim cpanel
            utils.lock_dkim(self.request.ldap)
            messages.success(self.request, _('Creando certificado dkim para el dominio %s' % dn))
            return HttpResponseRedirect(url)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            messages.error(self.request, _('Hubo algún problema creando el certificado Dkim '
                                           'Contacta con los administrador-s si el problema persiste'))
            return HttpResponseRedirect(url)

