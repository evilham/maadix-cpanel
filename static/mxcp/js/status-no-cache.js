var loginform = document.getElementById("form-ready");
var updatingform =  document.getElementById("form-updating");
/*
At the beginiing, the login form is invisible. Till a check against cpanel status
is performed end returns 'ready'
*/
//loginform.className += "hide";
var firstload=true;
var firstPath= get_installation_path();
var theUrl = firstPath + '/status-cpanel';
var oldDomain= getCookie('oldDomain');
var newDomain= getCookie('newDomain');
var theNewUrl = "https://" + newDomain + theUrl;
function httpGetAsync(theUrl)
{
    try
    {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4){
            /* Check if a result is given to avoid json error if no value is rtutned*/
            var response = (xmlHttp.responseText)
            if (xmlHttp.status == 200 && response == 'ready'){
              loginform.classList.toggle("hide");
              updatingform.className += " hide";
              
            }else if (xmlHttp.status == 200 && response != 'ready'){
              // If status not ready, go on with request
              if (firstload == true) {
                updatingform.classList.toggle("hide"); 
                firstload = false;
              }
              //setTimeout(httpGetAsync(theUrl), 3000);
              setTimeout(function() {httpGetAsync(theUrl)}, 3000);

            } else {
                // Can't call script from new fqdn - Blocked by CSP
                
                console.log('before sñeep');
                sleep(10000).then(() => {
                console.log('after sñeep');
                loginform.classList.toggle("hide");
                updatingform.className += " hide";
                })
            }   

        } 
          
    }
    xmlHttp.open("POST", theUrl, true); // true for asynchronous 
    var csrftoken = getCookie('csrfcpaneltoken');
    xmlHttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlHttp.send();
    }
    catch(err){

        console.log("cat error" + err.message);
        /* After old fqdn is not available anymore, wait befor showing link to new login
        */
        sleep(5400);
        loginform.classList.toggle("hide");
        updatingform.className += " hide";
    }

}
function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

httpGetAsync (theUrl);
