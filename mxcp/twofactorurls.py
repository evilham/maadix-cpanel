from django.urls import path

from two_factor.views import (
    BackupTokensView, DisableView, LoginView, PhoneDeleteView, PhoneSetupView,
    ProfileView, QRGeneratorView, SetupCompleteView, SetupView,
)

core = [
    path(
        'login/',
        LoginView.as_view(),
        name='login',
    ),
    path(
        'two-factor/setup/',
        SetupView.as_view(),
        name='setup',
    ),
    path(
        'two-factor/qrcode/',
        QRGeneratorView.as_view(),
        name='qr',
    ),
    path(
        'two-factor/setup/complete/',
        SetupCompleteView.as_view(),
        name='setup_complete',
    ),
    path(
        'two-factor/backup/tokens/',
        BackupTokensView.as_view(),
        name='backup_tokens',
    ),
    path(
        'two-factor/backup/phone/register/',
        PhoneSetupView.as_view(),
        name='phone_create',
    ),
    path(
        'two-factor/backup/phone/unregister/<int:pk>/',
        PhoneDeleteView.as_view(),
        name='phone_delete',
    ),
]

profile = [
    path(
        'two-factor/',
        ProfileView.as_view(),
        name='profile',
    ),
    path(
        'two-factor/disable/',
        DisableView.as_view(),
        name='disable',
    ),
]

urlpatterns = (core + profile, 'two_factor')
