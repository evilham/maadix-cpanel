# Local overrides

DEBUG                 = True
DEBUG_INSTALL         = True
LOCAL                 = True
CUSTOM_HOSTNAME       = 'custom_hostname'
CUSTOM_API_ENDPOINT   = 'http://localhost:9999/api/1.0'
FORCE_SCRIPT_NAME     = ''
SESSION_COOKIE_PATH   = ''
SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE    = False
SECURE_SSL_REDIRECT   = False
EMAIL_BACKEND         = 'django.core.mail.backends.smtp.ConsoleBackend'
STATIC_URL            = '/static/'
MEDIA_URL             = '/media/'
LDAP_AUTH_URL         = "ldap://1.1.1.1:389"

# django ldap auth
AUTH_LDAP_SERVER_URI  = LDAP_AUTH_URL
AUTH_LDAP_BIND_DN = "cn=doe,dc=example,dc=tld"
AUTH_LDAP_BIND_PASSWORD = "doe_password"
AUTH_LDAP_USER_SEARCH = LDAPSearch('')
)


# Apps
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'apps.views',
    'apps.templatetags',
    'django_otp',
    'django_otp.plugins.otp_static',
    'django_otp.plugins.otp_totp',
    'two_factor',
)
