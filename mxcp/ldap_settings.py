# The URL of the LDAP server.
# TODO: enable ldaps with TLS
import grp
SFTP_GUID  = grp.getgrnam('sftpusers')[2]
USER_SERVICES = ['openvpn', 'phpmyadmin', 'jitsi','sshd']
LDAP_AUTH_URL = "ldap://localhost:389"
# Initiate TLS on connection.
LDAP_AUTH_USE_TLS = False
# Set connection/receive timeouts (in seconds) on the underlying `ldap3` library.
LDAP_AUTH_CONNECT_TIMEOUT = None
LDAP_AUTH_RECEIVE_TIMEOUT = None


# LDAP tree
LDAP_TREE_BASE       = "dc=example,dc=tld"
LDAP_TREE_HOSTING    = "o=hosting,%s"             % LDAP_TREE_BASE
LDAP_TREE_USERS      = "ou=sshd,ou=People,%s"     % LDAP_TREE_BASE
LDAP_TREE_SERVICES   = "ou=groups,%s"             % LDAP_TREE_BASE
LDAP_TREE_SENDERMAIL = "ou=sendermail,%s"         % LDAP_TREE_BASE
LDAP_TREE_API        = "ou=api,%s"                % LDAP_TREE_BASE
LDAP_TREE_CPANEL     = "ou=cpanel,%s"             % LDAP_TREE_BASE
LDAP_TREE_REBOOT     = "ou=reboot,ou=cpanel,%s"   % LDAP_TREE_BASE
LDAP_TREE_DKIM       = "ou=opendkim,ou=cpanel,%s" % LDAP_TREE_BASE
LDAP_TREE_CLEAN      = "ou=clean,%s"              % LDAP_TREE_CPANEL
LDAP_TREE_TRASH      = "ou=trash,%s"              % LDAP_TREE_BASE
# LDAP filters
LDAP_FILTERS_SFTP                        = "(&(objectClass=person)(authorizedService=sshd)(gidnumber=%s)(!(gidnumber=27)))" % SFTP_GUID
LDAP_FILTERS_SSH                        = "(&(objectClass=person)(authorizedService=sshd)(!(gidnumber=27))(!(gidnumber=%s)))" % SFTP_GUID
LDAP_FILTER_ALL_USERS                   = "(&(objectClass=person)(uid=*)(!(gidnumber=27)))"
LDAP_FILTERS_USERS_WEBMASTER_FORM        = "(&(objectClass=person)(uid=*)(authorizedService=sshd)(!(gidnumber=27)))"
LDAP_FILTERS_USERS                       = "(&(objectClass=person)(uid=*)(authorizedService=sshd))"
LDAP_FILTERS_EMAILS                      = "(objectClass=VirtualMailAccount)"
LDAP_FILTERS_SUPERUSER                   = "(&(objectClass=person)(objectClass=metaInfo)(gidnumber=27))"
LDAP_FILTERS_SUDOERS                     = "(&(objectClass=person)(uid=*)(gidnumber=27))"
LDAP_FILTERS_INSTALLED_ENABLED_SERVICES  = "(&(objectClass=organizationalUnit)(status=enabled)(type=available))"
LDAP_FILTERS_INSTALLED_DISABLED_SERVICES = "(&(objectClass=organizationalUnit)(status=disabled)(type=installed))"
LDAP_FILTERS_INSTALLED_SERVICES_ALL      = "(|(&(objectClass=organizationalUnit)(status=enabled)(type=available))(&(objectClass=organizationalUnit)(status=disabled)(type=installed)))"
LDAP_FILTERS_REBOOT                      = "(objectClass=metaInfo)"
LDAP_FILTERS_DKIM                        = '(&(objectClass=organizationalUnit)(objectClass=metaInfo))'
LDAP_FILTERS_SENDERMAIL                  = '(&(objectClass=organizationalUnit)(objectClass=metaInfo))'
LDAP_FILTERS_DOMAINS                     = '(objectClass=VirtualDomain)'
LDAP_FILTERS_MAILMAN                     = "(&(vd=*)(accountActive=TRUE))"
LDAP_FILTERS_CLEAN                       = '(&(objectClass=organizationalUnit)(objectClass=metaInfo))'
# OTHER - these are for retreiving apps logos from an external source
ASSETS_URI    = "https://assets.maadix.net/"
IMAGES_FOLDER = "c-panel/images/services/"
