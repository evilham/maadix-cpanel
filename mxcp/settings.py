import os, sys, grp
from django.utils.translation import gettext_lazy as _

#
# DEBUGGING VARIABLES
#

LOCAL             = False
DEBUG             = False
NO_API            = False
NO_MAILMAN        = False
DEBUG_INSTALL     = False
FORCE_SCRIPT_NAME = '/cpanel'

#
# TLS
#

LDAP_AUTH_URL = "ldap://example.tld:389" 
# Initiate TLS on connection.
LDAP_AUTH_USE_TLS = True

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ENV_PATH = os.path.abspath( os.path.dirname(__file__) )
STATIC_URL = FORCE_SCRIPT_NAME + '/static/'
STATIC_ROOT = os.path.join(ENV_PATH, '..', 'static')
PROJECT_STATIC_FOLDER = 'mxcp'
STATICFILES_DIRS = [
    ( PROJECT_STATIC_FOLDER, STATIC_ROOT + '/' + PROJECT_STATIC_FOLDER + '/' ),
]
MEDIA_URL = FORCE_SCRIPT_NAME + '/media/'
MEDIA_ROOT = os.path.join(ENV_PATH, '..', 'media')
LOGIN_URL = 'two_factor:login'
#LOGIN_REDIRECT_URL = 'system-details'
LOGIN_REDIRECT_URL = 'loginredirect'
LOGOUT_REDIRECT_URL = 'logout'
CSRF_COOKIE_NAME = 'csrfcpaneltoken'
SESSION_COOKIE_NAME = 'cpanelsessionid'
LOCALE_PATHS = [
    BASE_DIR + '/apps/views/locale',	
    BASE_DIR + '/templates/locale',
]
#Other cusrom paths 
BACKUPS_PATH = 'home/backups'
#
# Permissions
#
ALLOWED_URLS = {
    'admin'      : [ '^(.*)$', ],
    'anonymous'  : [ 'favicon.ico', 'login', 'status-cpanel','status-cpanel-apache', 'changesystem', 'i18n', 'pass-recover', 'recover-confirm', 'password_reset', 'reset-success' ],
    'postmaster' : [ 'login', 'mails', 'email', 'profile', 'logout' ],
    'email'      : [  'login', 'email', 'profile', 'logout' ]
}
DATABASES = { 
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }   
}

#
# HTML
#
DOCUMENT_TITLE = "maadix"

#
# Application definition
#

# Apps
CONTRIB_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mod_wsgi.server',
)

PROJECT_APPS = (
    'apps.views',
    'apps.templatetags',
    'django_otp',
    'django_otp.plugins.otp_static',
    'django_otp.plugins.otp_totp',
    'two_factor',
)

INSTALLED_APPS = CONTRIB_APPS + PROJECT_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'apps.views.middleware.LdapConnectionMiddleware',
    'django_otp.middleware.OTPMiddleware',
]

# Session cookies security
# Set cookie age to 48h to avoid CSRF errors
SESSION_COOKIE_AGE = 1800 
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_PATH = FORCE_SCRIPT_NAME
SESSION_COOKIE_SAMESITE = "Strict"

# CSFR Cookie
CSRF_COOKIE_SECURE  = True
CSRF_COOKIE_AGE = 172800 
# Can't set httponly. Token is used by client-side JavaScript
# CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_PATH = FORCE_SCRIPT_NAME
CSRF_COOKIE_SAMESITE = "Strict"


#SESSION_FILE_PATH default value is /tmp...see if we want to change it
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
ROOT_URLCONF = 'mxcp.urls'

# SSL
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [ BASE_DIR + '/templates' ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.contrib.messages.context_processors.messages',
                'apps.views.context_processors.site_info_processor',
                'apps.views.context_processors.active_section_processor',
                'apps.views.context_processors.ldap_status',

            ],
        },
    },
]

WSGI_APPLICATION = 'mxcp.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 10,
        },
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
    {'NAME': 'apps.views.auth_validators.UppercaseValidator', },
    {'NAME': 'apps.views.auth_validators.LowercaseValidator', },
    {'NAME': 'apps.views.auth_validators.SymbolValidator', },
]

AUTHENTICATION_BACKENDS = [
    "django_auth_ldap.backend.LDAPBackend"
]

#
# Mail settings
#
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
DEFAULT_FROM_EMAIL = 'Maadix <no-reply@maadix.net>'
EMAIL_PORT = '25'
EMAIL_USE_TLS = True


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'es'
TIME_ZONE     = 'Europe/Madrid'
USE_I18N      = True
USE_L10N      = True
USE_TZ        = True
LANGUAGES = [
  ('es', _('Castellano')),
  ('en', _('English')),
]

# Important for making 2FA work with LDAP
TWO_FACTOR_REMEMBER_COOKIE_AGE = 1


from .custom_conf import *

""" Custom conf file included in gitignore that may be managed externally
"""
SFTP_GUID  = grp.getgrnam('sftpusers')[2]

from .ldap_settings import *

""" This is to generate a SECRET_KEY
    automatically, without having to install django on a separate
    way. if the file key_file.py containing the key doesn' exists, let's genereate it
    else just import it
"""

filename = PROJECT_STATIC_FOLDER + '/key_file.py'
if ( not os.path.isfile(filename)):
    print('missing keyfile')
    from django.utils.crypto import get_random_string
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    key = get_random_string(50, chars)
    key_string = 'SECRET_KEY = \'%s\'' % key
    try:
        with os.fdopen(os.open(filename, os.O_WRONLY | os.O_CREAT, 0o600), 'w') as keyfile:
            keyfile.write(key_string)
            keyfile.close()
    except Exception as e:
        print("There was a problem writing the keyfile", str(e), sep="\n")
        
from .key_file import *

# Local overrides
if os.path.exists('%s/mxcp/local_settings.py' % BASE_DIR):
    from .local_settings import *
