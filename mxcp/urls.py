# django
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.urls import path, re_path, include
# For paa revovery
from django.contrib.auth import views
from django.views.generic.base import TemplateView
#from django.contrib.auth import views as auth_views
from django.conf.urls import i18n 
# project
from apps.views import views_activate as activate
from apps.views import views_system as system
from apps.views import views_domains as domains
from apps.views import views_dns as dns
from apps.views import views_mails as mails
from apps.views import views_notifications as notifications
from apps.views import views_services as services
from apps.views import views_users as users
from apps.views import view_delete as delete
from apps.views import views_applications as applications
from apps.views import view_status as status
from apps.views import views_trash as trash
from apps.views import views_registration as registration
from . import twofactorurls as tf_urls

urlpatterns = [

    #path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    #path('password_change/done/', views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    #path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    #path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    #path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    #path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    #path('admin/', admin.site.urls),
    path('i18n/', include('django.conf.urls.i18n')),
    # Activation (first access)
    path('activate', activate.Activate.as_view(), name="activate"),
    # registration
    path('user-profile', users.Profile.as_view(), name="mxcpprofile"),
    path('', include(tf_urls.urlpatterns)),
    path('loginredirect', users.LoginRedirect, name="loginredirect"),
    # path('login', users.LoginView.as_view(), name="login"),
    path('changesystem', system.NewFqdnSet.as_view(), name="changesystem"),
    #path('password-recovery', users.PasswordRecovery.as_view(), name="password-recovery"),
    # Old URLS for recover pass
    #path('password-reset', users.PasswordReset.as_view(), name="password-reset"),
    #path('password-set', users.PasswordSet.as_view(), name="passowrd-set"),
    #path('password-resetconfirm', users.PasswordResteConfirm.as_view(), name="password-resetconfirm"),
    # New pass recover
    path('pass-recover', registration.PasswordResetView.as_view(), name="pass-recover"),
    path('recover-confirm/<str:uidb64>/<str:token>', registration.PasswordResetConfirmView.as_view(), name="recover-confirm"),
    path('pass-recover-done', registration.PasswordResetDoneView.as_view(), name="pass-recover-done"),
    path('logout', users.LogoutView.as_view(), name="logout"),
    path('reset-success', registration.PasswordResetSucessView.as_view(), name='password-reset-success'),

    # sections
    path('', system.Details.as_view(), name="system-details"),
    path('system/reboot', system.Reboot.as_view(), name="reboot"),
    path('system/update', system.Update.as_view(), name="update"),
    path('system/fqdn', system.Fqdn.as_view(), name="fqdn"), 
    path('system/config', system.SystemConfig.as_view(), name="system-config"),
    path('services', services.Services.as_view(), name="services"),
    # In progrees to replace service install view
    path('services/install', services.AvailableAppsview.as_view(), name="services-available"),
    path('services/settings/<str:appid>/', applications.AppSettings.as_view(), name='apps-settings'),
    path('domains', domains.DomainsListView.as_view(), name="domains"),
    path('domains/add', domains.AddDomainView.as_view(), name="domains-add"),
    path('domains/edit', domains.EditDomainView.as_view(), name="domains-edit"),
    path('domains/instructions', TemplateView.as_view(template_name='pages/domains-instructions.html'), name="domains-instructions"),

    path('mailman/domains', domains.MailmanDomains.as_view(), name="domains-mailman"),
    path('dns', dns.DnsView.as_view(), name="dns"),

    path('mails', mails.MailAccounts.as_view(), name="mails"),
    path('email/', mails.MailAccount.as_view(), name="email"),

    path('users', users.Users.as_view(), name="users"),
    #Don't change this uls name. It is used in views_users to check current url
    path('users/edit', users.User.as_view(), name="user"),
    path('users/superuser/edit', users.SuperUser.as_view(), name="superuser"),
    path('users/postmasters', users.Postmasters.as_view(), name="postmasters"),
    path('users/postmasters/edit', users.Postmaster.as_view(), name="postmaster"),

    path('notifications', notifications.Notifications.as_view(), name="notifications"),
    path('trash', trash.Trash.as_view(), name="trash"),
    # api
    path('api/delete-entry', delete.DeleteEntry.as_view(), name="api_delete"),
    path('api/get-cpu-usage', system.get_cpu_usage, name="api_get_cpu"),
    path('api/create-dkim', domains.CreateDkim.as_view(), name="api_create_dkim"),

    #ldap cpanel status
    # a link to services-install is harcoded inside static/mxcp/js/status.js
    path('status-cpanel', status.CheckCpanelStatus.as_view(), name="status-cpanel")
    ]
